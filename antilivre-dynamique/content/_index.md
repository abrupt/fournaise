---
title: Fournaise
---

<div class="texte" data-name="Sans emploi">

<div class="a-droite dedicace">

*si cela m'arrive de parler des étoiles, c'est uniquement par mégarde*

--- Samuel Beckett

</div>

## Sans emploi

Et c'est ainsi que j'arrivai à Nîmes. À Nîmes, il n'y a pas la mer. Nîmes est la ville la plus chaude de France, etc. En vérité, je n'en sais rien. Je sais seulement que l'été nîmois est éprouvant.

Quel jour sommes-nous ? Les dates me raccrochent à une biographie déliquescente. Tout ce que j'ai écrit et dit, je le renie. J'ai été marxiste, c'est ma dernière loyauté. Mais je le serai désormais d'une façon plus barbare, comme tous les orphelins.

Orphelin deux fois. Car je le suis également d'un groupe de camarades --- *allumer des clartés*, disait-elle, *le long de mes rues mortes*.

Ce qui est beau, avec un groupe de camarades, quel qu'il soit, c'est qu'on peut le renier, s'en exclure, en être exclu --- c'est un jeu de forces qui nous maintient en vie. Le vieux monde suffoque, continue de dicter la cadence.

J'en veux terriblement aux mots de nous maintenir en apnée. Alors que le monde vacille. Alors que je ne fais nulle part, ou mal, ou trop peu --- l'expérience des choses.

Quelle idée de foutre du minéral blanc partout. Le ciboulot déraille, il ne fonctionne que --- par intermittence.

Les perspectives concrètes ne se bousculent pas. Il faut en prendre soin. Les blottir contre soi. Tout leur concéder. Notre abandon le plus viscéral. Si on hésite on reste couché.

C'est la première fois que je ne sais pas où je vais. J'allais dire : où je cours.

À vrai dire je m'y dirige plutôt tranquillement, sans forcer le pas. D'autant que mon torticolis réduit considérablement mon champ de prospection, ainsi que ce lumbago mesquin qui me tiraille, et cette perfide contracture dont on m'assure qu'elle est forcément liée à une première injection de vaccin Pfizer inoculée hier --- je suis donc parfaitement inapte à la course à pied.

Et dire que j'ai toujours marché d'un pas très vif. Dans le temps, c'était même une marque de fabrique. Dans le temps, quand j'avais des camarades et des tracts et l'aube du lendemain et cette vieille rengaine apprise à l'école du Parti. Dans le temps, je marchais d'un pas vraiment vif. En me foutant, coquin d'Icare, de la destination. Mais aujourd'hui je lève le pied, comme disent les analystes. La différence notable, dans mon cas, c'est qu'on attend rien de moi.

Écrire de belles choses, peut-être, pas trop biscornues, digestes, des choses qui passent crème, sans vague, juste le petit ressac de l'aube.

Ah non ! ce n'est pas moi qui souhaite ça. Moi je veux me perdre dans l'inconséquence et le trouble. Me trouver un abri au cœur de l'orage.

Mais alors, si ce n'est pas moi qui le dis, qui est-ce ? Je concède que tout ceci m'angoisse. D'autant que je suis très maladroit avec les enfants.

J'aimerais me réveiller sur une plage. Face contre mer. Ses mouvements incessants, ses humeurs, son implacable ponctualité. Va-&-vient assourdissant. D'autres auraient pu dire : rédempteur. Mais je n'aime pas le clafoutis, surtout en matière de littérature.

Je me contenterai donc de cette odeur d'oubli, de sel, d'algue, de poiscaille, de sable. Et puis la mer s'en contrefout de nos horloges, de Nîmes, de la chaleur, de mes phrases biscornues. On dit même qu'elle mange ses enfants. Chut.

D'accord mon amour : j'oublierai la mer, la montagne, l'Italie, et même la littérature, afin de me consacrer à l'acte de reproduction de l'espèce, moi qui me contrefous de l'espèce, comme des mille variétés de poissons dont je ne retiens jamais les noms.

De façon générale, je connais mal le nom des choses. Mon vocabulaire ne me permet de décrire que des courbes abstraites, des humeurs, des faillites. L'absence, surtout. C'est joli l'absence.

J'ai beau tenter de fixer mon attention sur le rire des enfants --- on dit que c'est très beau un rire d'enfant --- je flanche. Et me voici de nouveau errant dans les souterrains d'une ville imaginaire.

Que voulez-vous ? Je ne suis inspiré que par la perte et les immeubles gris. Rien de bien épais, je le concède, sur le plan ontologique.

À ma décharge, je connais des concepts à foison. La faute à une formation philosophique plutôt rigoureuse. Il est temps d'en faire quelque chose, me suggère un passant.

J'ajoute que je connais des paroles de chanson. Jusqu'à l'écœurement. Bref : rien que des manières de dire, non de faire. Je ne sais rien faire du tout. Tu me le reproches souvent, tacitement, tout le temps. Et je t'en veux car tu as raison.

Je suis ingrat et orgueilleux. C'est qu'il n'y a rien de ce que je fais ou dis --- travestissements, pastiches, inconséquences --- qui ne mêle l'os de mes jours à tes couleurs vaillantes. En dernière instance : tout te revient. Le gras et la chair travaillée, onctueuse.

Dire ce n'est pas faire. La belle pirouette que voilà. Le linguiste se mord les vertèbres. Le lacanien ricane.

Je ne sais rien faire du tout. Et ma capacité à dire n'y peut rien, c'est foutu. Aucune compétence technique. Depuis que j'ai renoncé à l'Histoire et aux apothéoses, je me bringuebale mon ennui aux quatre coins d'une ville dont je ne sais rien.

D'ailleurs : suis-je à Nîmes, déjà ? Je commence à en douter. Mais je sais que nous sommes le 8 juin 2021 et qu'il commence à faire très chaud.

Je sue énormément et glisse sur mes sandales en plastique. Ayant rendez-vous chez l'ostéopathe, je sais déjà que je me sentirai honteux d'arriver en sueur dans son cabinet. Alors je m'excuserai et cela n'aura d'autre utilité que de fixer son attention sur ma sueur. Ce qui est très bête car cela risque de la distraire. Alors qu'il y a tant à faire avec mon torticolis, mon lumbago, etc.

Si j'emploie des termes médicaux, c'est pour donner de l'épaisseur --- la fameuse --- à mon récit, afin d'amplifier ma confession. C'est plus sérieux ainsi. Et puis les narrations réalistes, pleines de listes à rallonge, de sueur et de technicité, je sais que ça en impose. L'obsession d'un auteur s'y découvre.

Il est vraiment dommage que les obsessions les plus manifestes des auteurs contemporains se traduisent dans les tergiversations de leur ego. Émaillées de petits cris vermeils, bien sûr, de cette rondeur veloutée et crispante qu'accompagne la digestion d'un repas saturé en mauvaises graisses.

On y apprend beaucoup moins de choses que dans un roman de Mann (anatomie), Joyce (sexualité), London (la chasse aux phoques). Ce sont des exemples arbitraires. Et prétentieux, diront mes congénères.

Il est quand même triste et déplorable d'écrire afin d'être confirmé. Dans sa plume, comme un écolier. Il y a vraiment quelque chose de sinistre dans la condition d'auteur. Ce qui donne à penser --- mais moi je ne pense pas, je fais semblant --- que nous n'écrivons pas, que nous obéissons.

<div class="centre ligneblanche">

LISTE DE COURSES

&nbsp;

aulx x 2

oignons doux (gros) x 4

patates (un demi-kilo)

jus (pruneau ou raisin)

fraises (si elles sont belles)

blettes x 2

pasta (capellini)

crème fraîche (entière)

fromage pasteurisé (comté, emmental)

yaourt citron

lentilles

pois cassés

haricots rouges

pois chiches

</div>

J'ignore pourquoi on s'obstine à cuisiner des pois chiches. Cela me donne des flatulences carabinées. Et j'ai l'impression que ça la fait rire. Mais je ne lui en veux pas. Il faut préciser, afin de ne pas la peindre plus mégère qu'elle n'est, qu'elle aime sincèrement les pois chiches, qu'elle ne les sacrifierait pas pour me reposer l'estomac et l'anus, c'est tout. Ce qui s'entend très bien.

La vie n'est qu'un amas de petits inconvénients logistiques. Considérés isolément, analytiquement, ils seraient parfaitement dérisoires. Considérés de façon globale et synthétique, c'est un calvaire.

Et dire que je n'avais aucune intention de vous parler de mes flatulences. Ni du covid. Ni de la détresse des petits commerçants.

J'insiste : maintenant que les terrasses ont rouvert, on ne va pas se raconter nos petites déconvenues, n'est-ce pas (?), nos petites hontes, ces sanctions du réel qui nous accablent, surtout l'été, quand il fait chaud, très chaud, et que nous nous rendons chez l'ostéopathe.

--- Le quartier Richelieu, ça craint !

--- Ah ouais, ça craint tant que ça ?

--- Bah ouais, puisque j'te le dis.

--- OK.

De toute façon, il faut noircir du papier. Sans penser à rien. Penser ça fait mal. Et puis ça sert à rien. Depuis le temps qu'on me le répète. Je me souviens : adolescent, jeune adulte, il y a pas si longtemps que ça, quand je fonçais poing dans la cohue, afin de défendre l'Idée, le Projet, l'Histoire, la Nécessité, etc. Depuis quand ai-je cessé d'y croire ? L'infamie du quotidien, sans doute.

Oh, pas la grande épreuve. Pas encore. Un enchaînement de petites contrariétés mesquines : dettes à la consommation, budgets prévisionnels, premières cuites solitaires.

Ce n'est pas faute d'avoir essayé : psychanalysme, surréalisme, communisme, etc.

Prenez les chevaliers de l'arc-en-ciel, par exemple. Un vrai mouvement celui-ci. Ils habitent les hauteurs du Vigan. Ce sont des manuels, des gens qui travaillent de leurs mains, savent construire des cabanes, des fours à pain, qui connaissent les spécificités des saisons, ne mangent pas des fraises en décembre, etc. Eh bien eux, ils puent vraiment des pieds. Plus que moi, ça ne fait aucun doute. Je n'ai jamais compris pourquoi. Une histoire d'hormones et de tout-à-l'égout défaillant, semble-t-il. C'est la boulangère de Ganges, un bled d'à côté, qui me l'a dit. Et je la crois, figurez-vous, ses yeux bleus m'ont souri.

À Tain-l'Ermitage, on gifle les institutions. On se connecte aux réseaux, pas les mainstreams, un truc de geek, et on visualise en boucle la gifle donnée à la République. Et on rit et on rit.

Les flux de conscience sont fissurés, altérés, fragmentés. La narration classique n'a plus prise sur nos réalités éclatées. Pour autant : réhabiliter la grande narration classique, c'est coucher avec le fantôme de l'Occident.

Calme et placide, j'en redemande. Et on rit et on rit.

Le bonheur existe-t-il autrement que comme acceptation, plate et lucide, de nos natures encombrées d'absence et de sottises ?

Je proteste : nos gueules de cafards, de chômeurs, priés de s'étourdir dans le remplissage administratif, dans la recherche d'un emploi, d'une place, dans l'assignation à un profil, dans la mise à jour de nos situations, etc. Mais la rancœur du vide pèse des tonnes. Dilapider pesanteur, fausses lumières --- oserais-je dire : de nos grâces ?

La grâce est sans emploi, par nature, inemployée, inemployable. Elle s'affranchit ainsi de toute pesanteur. Et bien qu'elle lui soit liée nécessairement, comme la faute à sa réparation. Mais joyeuse est notre réparation. Aucune culpabilité morbide. C'est une disponibilité pure, sans objet préalable, sans finalité. Une éternelle chômeuse errant désœuvrée en quête d'un simple regard.

"Tout vide (non accepté) produit de la haine, de l'aigreur, de l'amertume, de la rancune." Simone Weil, *La pesanteur et la grâce*.

Merde, alors il faudrait rééduquer tous les experts en management, les formateurs, les éducateurs, les prédicateurs, leur faire bouffer du geek et du margoul jusqu'à l'indigestion, les regarder dégueuler nos gueules de cafards, nos manières louches, nos scintillements précaires, nos pesanteurs, et nos grâces.

Le vin coûte moins cher à Nîmes qu'à Marseille. Mais je suis devenu exigeant. Le Costière abîme les dents. Comme l'acier. Ne plus s'abstenir devant la possibilité d'un raffinement.

<div class="centre ligneblanche">

CECI EST LA CONFESSION D'UN PARASITE

</div>

J'ai jeté mes derniers tourments dans une marmite, porté l'eau à ébullition, et j'attends patiemment que le jugement de Dieu me distingue.

Pendant que la marmite chauffe, que l'eau parvient fatidiquement à température de 97 degrés, je contemple ces petites bulles de calcaire qui vrombissent dans une tempête qui emportera tout, geeks, margouls, etc.

Décidément, les perspectives raisonnables sont des feuilles mortes. Il faut réapprendre à nous toucher. Du bout des yeux. Nous sommes neufs dans le vieux monde.

*Note 1* : distinguer le relâchement de la tension nerveuse et la caresse chargée d'un érotisme qui considère le moyen comme une fin. Reconsidérer les moyens.

*Note 2* : je me demande ce que Dewey & Trotsky auraient à dire de l'érotisme. J'ai passé ma vie à envisager les fins. J'aimerais me concentrer sur les moyens. Il va falloir se remettre au boulot. Je veux dire : sérieusement.

<div class="centre ligneblanche">

<div style="margin-right: 8em">ICI</div>

<div style="margin-right: 4em">DESSINER</div>

<div style="margin-left: 4em">QUELQUE CHOSE</div>

<div style="margin-left: 8em">D’AMUSANT</div>

</div>

Je n'aime pas beaucoup l'idée de confier mes crasses à un bout de papier. Me vautrer sur la page, littéralement. Chercher la singularité la plus absolue dans l'impersonnalité la plus flagrante. Ce délit de poireau, cet esquif. Rire de l'osier et du pourpre de ces carrossiers du divin. Bondir sous la lame aiguisée de l'inconsistance.

Amplitude, précision. EN REJETANT RADICALEMENT LA POSSIBILITÉ D'UNE IDENTIFICATION.

Ce pour quoi j'aime tant les histoires de duchesses.

Menteur ! Qui ne s'est jamais identifié à la protagoniste d'un conte crépusculaire, avec ses paillettes et ses princes, ses valses et son champagne ?

Dans la grotte de Ludwig, je reconnais qu'il m'est arrivé de coucher quelques promesses : *Devrais-je me pendre aux astres,* / m'écriais-je, *au déluge à la fête,* / *pour faire valoir* / *mon droit à la tendresse ?*

Mes états d'âme se diluent dans un bruit de trompette, finissent par disparaître, ou par muer, car ils ne disparaissent pas vraiment, ils muent, et leur décor aussi : théâtre miniature et mesquin, *Nîmes Ville ouverte*, petite Rome avec ses sept collines.

Ce sont les mots qui manquent. La grammaire nous embourbe, rideau.

Ne reste que le langage, involontaire, brusque : celui du monde, de son absence.

Se rassurer dans une féerie bien épaisse, bien matérielle. S'y cogner le front très fort. S'étourdir un bon coup. Quand le vin coule à flots, s'y noyer. Rester sobre sans être pédant. C'est le problème avec Socrate.

Et alors : des chants qui ressemblent à des chants pyrénéens, à des hymnes mortuaires. Et l'obscurité qui remplit la nuit, toute la nuit, toutes les rues, sans le moindre scrupule, une prouesse de remplissage. Et l'aube qui vient percer l'édifice de la nuit par à-coups. Des ombres chancelantes. La lame du sculpteur qui persévère. Persévérance sans objet. Acrobatie stérile, obstinée.

Se confronter à la tangibilité du sol, un carrelage d'époque, années 30 je crois. Enfin je n'en sais rien. C'est histoire de noircir ma plaie, d'habiter mon petit théâtre, mon petit drame, ma petite enfance.

Aucune chance d'habiter le vide avec élégance. On a beau prendre un air sérieux, inspiré, comme si les idées s'entrechoquaient dans notre ciboulot en petits scintillements dialectiques... On a beau faire, on triche.

Il n'y a plus rien dans notre ciboulot. Plus rien. Plus rien d'articulable à la grande Histoire. Plus de vaincus, plus de vainqueurs. Nouvelle préhistoire.

Pas l'once d'une dialectique à l'horizon. Pas l'once d'une résolution de nos petits conflits intérieurs. S'en remettre aux thérapies comportementalistes, leur offrir nos dysfonctionnalités en pâture, nos désespoirs cliniquement diagnostiqués, nos pets mal négociés.

La vie est un œuf au plat.

Un ami juif --- je précise juif car mon ami est pratiquant et lit les psaumes de David dans le texte --- m'a appris hier que les œufs doivent se manger agrémentés de sel, sinon cela signifie que nous sommes en train de porter le deuil.

J'ai beaucoup de mal à me passer de sel et d'ail. Sauf quand je suis très fatigué.

Je crois qu'il faut continuer à écrire. Écrire tant que meurt le jour. Qu'il meurt dans le cœur hébété des enfants.

Écrire jusqu'à déraciner le sens : un bon désherbage, bien propre, binette, grelinette, etc.

Dépoussiérer la langue. Naître. Et alors : vieilles couleurs, ciels éteints --- accueillir l'inconnu, une natte et du pain chaud.

Ma propension à l'idéal, à l'épurement, à la chose vraie, ne serait-ce ce que j'ai de plus totalitaire en moi ? Mon curseur idéologique est un aiguillon. Et je ne voudrais pas dérouter mes camarades. J'en ai tellement. De moins en moins, il faut bien l'admettre. Mais il m'en reste quelques-uns et j'aimerais bien les décevoir.

Allons nous frotter aux cocos d'ici. Au Prolé, ça picole sec. J'en oublierai mon anxieuse paternité en jouant à fabriquer du discours.

*Tomber* : amoureux, ivre, sur un bout de trottoir, un week-end de fête taurine.

C'est un devoir d'écrire jusqu'à épuisement. Écrire pour écrire, sans se soucier des symétries, des raffinements, des enjambements. Écrire pour emplir toutes les arènes de Nîmes et faire danser les taureaux de Camargue. Écrire pour cesser de se lamenter. Écrire pour ne rien dire. Simplement parce que tu fais semblant d'être payé pour ça.

Comprenez-vous ? C'est la question du pouvoir qui me hante. Littérature et paternité : double gestation. Deux façons d'installer sa torpeur au sein d'un arbitraire de classe. Jouer à se distinguer, c'est affreux. Est-il encore possible d'écrire, d'être père, sans bousiller personne, sans exploiter personne, sans renier personne ?

Le tout, c'est d'écrire tous les jours. Sans se relire. Les livres se font ainsi, par mégarde, dans l'hystérie des jours malades.

Et alors : il se peut que Nîmes s'ouvre sur des ruelles affamées, des cabales, des marchés aux fèves, etc.

Trouver l'état de décomposition le plus jubilatoire dans la plus simple tâche, sans cesse recommencée, minutieusement accomplie. Nous aurons bien le temps de jouer aux orfèvres. Désormais il faut forger : tâche musculaire, pleine, entière. Les raffinements viendront plus tard, pour asséner la touche finale.

J'ai écrit un roman. Les romans m'épuisent. Si je veux une bonne intrigue, une bonne histoire, je relis Agatha Christie.

Mon roman s'intitule *La Soif*. C'est l'histoire d'un masticateur pathologique en quête d'absolu. Celle de tous les romans du monde, non ? C'est fatigant l'absolu, ça empeste.

Nous avons tous un roman non publié dans un placard : cet adolescent en nous qui se lamente et crache sur ses semblables, un visage braqué sur des lunes déloyales, sentiers de ronces et d'aubépines, sous des couchants précaires. Frénétiques couchants.

Tenir enfin les comptes de sa béatitude, dresser son premier bilan : des hordes d'experts-comptables se tirent la bourre, je suis propre, je sens le crédit et le bon travailleur.

C'est dégoûtant. Je n'en peux plus de nuire à ma vocation. Reprenons :

1.  respirer par le ventre
2.  mettre le feu au placard
3.  s'endormir avec le couchant

Cette nuit, je me suis noyé dans la crème épaisse d'une laitière, laitière de sa vocation. J'aime traire, est-ce une faute ?

ÉVITER LES MENSONGES DE LA QUESTION DU SENS, pour l'instant.

Une pulsion éradicatrice à l'œuvre dans tout modernisme : gratter, essuyer, repeindre. Faire le coq.

Je me jette sur le boulevard des sages. Toute la gôche chante en cœur. Je ne voterai pas pour les cantonales.

Aucune justification admise. Le vide le plus terrifiant. Et ce culte du remplissage : élections, yoga, cocaïne. Nouvelle trinité.

Camarade-lecteur, j'aimerais beaucoup que nous nous mettions d'accord sur un point : ici, NOUS N'ÉCRIVONS PAS D'HISTOIRE.

Une histoire suppose d'aller quelque part. Nous n'allons nulle part. Une histoire suppose un commencement. Nous ne commençons rien, nous insistons. La main tremblante, nous affirmons notre puissance de dessaisissement, de nuisance.

Nous attaquons là où ça nous prend. Tout ce qui importe c'est de refuser les histoires. Rien de ce que nous gravissons ne doit avoir l'allure d'une dérobade.

Ne plus rien contester au chaos. Y plonger. Assumer la profusion, l'épaisseur de nos épopées périphériques.

Commencer là où nous avons été jetés. Ne pas s'extasier. Ni sur le sens des événements ni devant le parfum d'étrangeté des choses. L'étrangeté, ça donne la nausée.

Toute littérature est une imposture. Toute littérature raconte une histoire. Il ne faut plus raconter d'histoire.

Haïr les commencements. Haïr les dénouements. Haïr toutes les morales.

Aimer les aisselles de son amour, ses rides précoces, sa tendresse obscène.

Se laisser happer nu par le silence.

Feuillages secs et persistants, *notre garrigue*.

## Document 1

### Modèle A : le roman pessimiste

1.  Un homme veut tout casser.
2.  Il casse tout.
3.  Il meurt car il a tout cassé mais n'a aucun regret : il n'y avait rien de mieux à faire.

### Modèle B : le roman optimiste

1.  Il y a des problèmes dans la vie de quelqu'un.

2.  Quelqu'un se sort des problèmes et c'est très héroïque.

3.  Il n'y a pas de troisième partie : ce qui importe c'est la lumière même quand il fait gris.

<div>&nbsp;</div>

ALTERNATIVES CONNUES : le réalisme socialiste, critique, surréaliste, viscéralistes, etc.

</div>

<div class="texte" data-name="Vallée fantôme">

## Vallée fantôme

et les océans et les mers et les fleuves et les fleuves côtiers surtout c'est important les fleuves côtiers ce sont de petits fleuves que j'apprécie tout particulièrement les fleuves côtiers donc et les rivières et les sources car nous refaisons le chemin à l'envers en quête d'un trésor de l'ère minière d'une locomotive d'une marquise col de l'exil col de Saint-Pierre col col et recol jusqu'en Lozère où de sympathiques militants de groupuscules abracadabrantesques lisent des revues complotistes où il faut attendre 1h du matin pour assister à une discussion raisonnable depuis que la lutte a pris l'accent des ragots je déchante je fais le tour des cabanes en Vallée française en quête d'un bon pinard et d'une entrevue plus clémente pour mes nerfs mais dès que je me trouve confronté à une discussion politique tenue par des énergumènes en voie de décomposition idéologique c'est plus fort que moi j'ai le marxisme pavlovien et de nouveau j'en veux aux étoiles d'avoir ramolli les ciboulots de cette jeunesse laborieuse nous sommes des millions d'anticapitalistes stériles camouflés sous des tonnes de ragots pendant que le Capital continue sa course aveugle comme un chien fou entraînant nos traditions vivantes vers la destruction obscénité de nos solitudes outrance de nos révoltes vieux tango des époques perdues flamme-fossile dans le musée de nos torpeurs petit marché provençal où la botte de lavande est au prix d'un plein de SP95 c'est mon unique référence car je n'ai pas le permis seulement un 50cm^3^ avec lequel je traverse des vallées avec lequel je roule aussi vite que je peux c-à-d à moins de 70 km/h et c'est tant mieux car dès que je dépasse les 70 km/h j'ai peur je ne supporte pas la vitesse c'est comme le whisky irlandais je triche car j'ai honte de ne digérer ni l'un ni l'autre ce qui ne m'empêche pas d'être épique quand je suis seul perdu entre deux vallées de Lozère quand je suis nu dans une rivière glacée celle de la Vis par exemple entre Ganges et Saint-Laurent-le-Minier ou encore quand j'écris pendant cinq heures sans discontinuer dans un petit café où on me prend pour un dératé car je tape du pied au rythme de l'écriture je ne maîtrise pas le rythme de mes pieds je ne veux pas maîtriser le rythme de mes pieds je veux qu'il me prenne par les pieds le rythme le rythme est la seule chose qui me permettra d'oublier que j'ai peur en voiture que je ne digère pas le whisky irlandais que les nouveaux maquisards sont vraiment des abrutis de complotistes où est passée la glorieuse camaraderie paysanne ce pragmatisme de camisard je crois que toute rencontre est affaire de rythme je ne veux plus fabriquer du discours je crois que nous pouvons très bien nous en passer surtout quand on est seul surtout quand l'époque est plombée je me souviens de ce théâtre des opinions dissidentes des marxismes itinérants des anarchismes réglementés des luttes à mort finissant dans le Vieux-Port ou dans une bonne et grasse métaphore rouillée décidément les astres tous les astres m'ennuient drôle de théâtre non dénué de charme après tout mais efficacité toute relative discours embourbé dans la certitude d'une émotion-fossile comme cette flamme tournant et tournant de plus en plus fragile avant de s'éteindre au cœur de la Vallée française au cœur des Cévennes cette flamme qui tourne une dernière fois sur elle-même avant d'emporter dans son extinction les dernières douleurs les frasques et nos ciboulots de complotistes jusqu'à disparaître cette flamme dans le silence qui broie toutes les langues toutes les images tous les complots le silence qui est un poing enfoncé dans la gorge du lyrisme et de l'effusion théorique le silence qui abasourdi le silence qui déchire le silence qui ignore les commencements les dénouements les fins le silence qui n'est qu'un grand dénuement pudique raffiné sauvage le silence qui n'abolit pas la lutte des classes mais la régénère lui donne de nouvelles armes rien de grand dans le monde ne se fera sans silence et sans lutte des classes rien de grand rien de juste rien qui vaille la peine de s'étourdir dans le bruit et les palabres rien qui vaille rien que le silence et tant pis pour cette petite flamme spectrale j'ose croire que le silence nous permettra de poser nos grosses mains velues sur autre chose que ces pauvres étoiles qui n'ont rien demandé et subissent depuis la nuit des temps la loi de nos détresses congestionnées de nos petites fissures de notre grande lâcheté le silence qui brave toutes les lâchetés qui apaise nos nerfs qui revivifie nos tissus musculaires le silence qui nous interdit nous repose nous régénère le silence qui fomente de nouvelles insurrections dans ses broussailles dans ses épis de glaire et de rosée le silence cet asile cet art de la guerre cet implacable refus cet art minutieux du défrichage désherbage effeuillage éradiquer herbes voraces invasives les silhouettes le bruit les bibelots de sa grand-mère inventer d'autres cartographies coquelicots tenaces en bordure de N106 vieux chemins de fer vestiges d'antiques vallées minières collines en forme de dinosaures au-dessus de Saint-Hippolyte-du-Fort nos pirateries juvéniles la quête des dernières asperges sauvages de la saison la Vis se jetant dans l'Hérault juste au-dessus de Ganges et puis en descendant vers Brissac l'Hérault toujours ayant pris l'ascendant sur le Rieutord et ces routes ombragées en forme de couleuvres ces formations rocheuses qui nous rappellent des territoires que nous ne traverserons jamais l'Arizona l'Arkansas un pont suspendu entre des continents imaginaires un infect tenancier de guinguette le litre de mauvais rouge à un prix raisonnable et dans le centre de Saint-Bauzille-de-Putois la caféine le foot de vieux ivrognes et leurs plaidoiries insensées en faveur des frontières mais de nouveau l'Hérault notre poème direction le Pic-Saint-Loup de nouveau des paysages de western de nouveau la sécheresse de nouveau la route de Montpellier rocades en tôle et béton armé Montpellier est une aberration esthétique tant pis la Méditerranée peut-être et Sète dernier tour de piste la tramontane s'accroche et persévère dans le mauvais alcool dans la sueur nos peaux collent à la nuit climat de cendre et d'infamie mais de l'autre côté là-bas très loin j'allais dire du rêve la faute à mon déficit momentané d'imagination à mon impatience aux abstractions que je me cogne depuis ma découverte de la philosophie toutes mes névroses en découlent et j'ai horreur de l'avion prudence on murmure que les gitans de Sète préparent une vendetta un gamin de Ganges s'est fait planter hier à l'angle de la rue Jean Jaurès et de la place de la mairie une terreur ce gamin trafic en gaz de protoxyde d'azote tous les anges sont terribles que faire nos peaux ont essuyé tant de dilemmes et de béton armé dans les eaux glacées de la Vis sur le lit asséché du Rieutord en bordure de l'Hérault route des grands Causses en direction de Saint-Guilhem-le-Désert ou à Issenssac sous cette chapelle surplombant des corps abrutis par la chaleur étendus comme forces muettes sur d'antiques rochers où des familles entières assument échéances d'écorces et de lièvres obstinément s'entailler la logique et le nerf n'avoir aucun scrupule à mourir avec son époque à s'enterrer vivant sous les décombres tristes sires abstenez-vous de juger nos petites excentricités lyriques c'est au silence de le faire je ne dis pas aux experts ou aux masses mais au silence l'implacable saturé persistant toute l'exploitation du monde n'annihilera jamais la bravoure d'une grève ou d'un amour gratter sec l'écorce des choses un lièvre passe silence

## Document 2

Je suis arrivé à Ustica le 7 décembre après un voyage plutôt pénible mais fort intéressant. Je suis en excellente santé. Ustica sera pour moi un séjour assez agréable du point de vue de l'existence animale, car le climat est excellent et je peux faire des promenades très salutaires : pour le confort général, tu sais que je n'ai pas beaucoup d'exigences et que je peux vivre avec le minimum. Ce qui m'inquiète un peu, c'est le problème de l'ennui, qui ne pourra être résolu uniquement par les promenades et par le contact avec les amis : nous sommes jusqu'à présent quatorze amis, dont Bordiga. Je m'adresse à toi pour que tu aies la gentillesse de m'envoyer quelques livres. Je souhaiterais avoir un bon traité d'économie et de finances à étudier : un livre fondamental que tu pourras choisir comme tu l'entendras. Quand cela te sera possible, tu m'enverras également quelques livres et quelques revues de culture générale, ce que tu jugeras intéressant pour moi. Très cher ami, tu connais ma situation de famille et tu sais combien il est difficile pour moi de recevoir des livres d'autres personnes que de quelque ami personnel : crois bien que je n'aurais pas osé te causer un tel dérangement, si je n'y étais poussé par la nécessité de résoudre ce problème de l'abrutissement intellectuel qui m'inquiète particulièrement.

</div>

<div class="texte" data-name="Os">


## Os

### Mistral

Cahots du lendemain, soupirs de l'Idéal : le vent a tout emporté.

### Rallonges

Écrire nous a simplement aidés à supporter tout ça, à étendre un peu nos existences --- à la manière de ces rallonges qu'on ajoute aux tables afin d'accueillir du monde.

### Pour le moment il était encore libre

Rien de plus mesquin que la satisfaction corrélée à l'achèvement d'une œuvre littéraire : l'inachèvement est un don de l'informe à l'objet lui garantissant une pérennité possible dans des mondes à venir ; c'est également une reconnaissance de l'intelligence du lecteur, et donc une relégation de la figure de l'auteur. Préservons l'inachevé.

### Chant

Une fois que le truc est ficelé, prier pour que personne ne l'ouvre : qu'il s'évanouisse dans un cri, qu'il appartienne aux limbes --- si nous vivions époque plus lumineuse, moins grasse, j'aurais peut-être osé le nom de chant, un chant jeté vers d'autres ciels, d'autres enfers.

### Postérité

Seule la poussière anoblit l'arrogance d'écrire.

### Chimie

Remplacer les bibliothèques personnelles par de toutes petites valises : mobilité et condensation (en un sens purement chimique) étant les principales caractéristiques du lecteur émancipé. *A contrario* : inertie et dispersion caractérisent l'usager du roman contemporain.

### Trieste

Saviez-vous que le camarade Joyce se destinait à être chanteur ? Ce prosateur sec, contempteur des idioties romantiques, qui se mettait méthodiquement les doigts dans la bouche pour éviter de brailler, écrivait donc des chansons lyriques, avec vent, carrousel, bord de mer, course de voiliers à Trieste.


### Trieste II

Ciel sans reflet, brasier amer, astre sans scrupule\
Qui déborde l'Orient,\
Et de l'espace sauvage, épuisant les contours,\
Nous sommes des faucons sans histoire.


### Dans la fin était le commencement

La fin de la *Recherche du temps perdu* est une invitation à reprendre la lecture depuis le début : non pour affûter notre compréhension du texte, mais pour éprouver la joie de s'attarder quelque temps dans un monde sans Dieu.


### Marx + Beckett

= une armée de vieux révolutionnaires suçant des cailloux dans une forêt obscure.


### Justification

C'est la cruauté du monde qui nous oblige à défaire la langue renversant ainsi l'oppression subie en acte de liberté.


### Sortie de route

Le poème peut bien se passer des autoroutes et des balises de la narration. Nous nous foutons de savoir s'il trace un sillon praticable, c'est un chemin de pure lumière, perdu parmi les choses. Notre revanche sur la vie pratique.


### Inspiration (?)

Se remplir d'hirondelles tuberculeuses de cartouches de vieux fusils rieurs de soleils cassés d'angles morts. Ne pas croire en l'inspiration : se remettre au travail, vite vite, jeter braises dans nos inventaires.


### Géographie(s)

Éviter les simulacres de la sublimation : se trouver une géographie suffisamment concrète et insensée pour manifester la pure présence.


### Langue

Laisser sa chance à l'inavouable : cri de tourbe dans le désert de la posthistoire, confluence de boue et d'eau, nos langues de marécage.


### Situation

Un type qui s'installe en terrasse d'un PMU pour pianoter cinq heures sur son ordinateur, enchaînant compulsivement les cafés, c'est nécessairement un parasite, un assisté, sûr que son père a de la thune, qu'il vit sous assistance matérielle de l'État. À la bibliothèque, c'est plus convenable, le décor sied à l'action. Imaginez un gars qui installerait un four à pain dans une bibliothèque, on le prendrait pour un illuminé. À Nîmes, écrire dans un café, c'est pareil.


### Place de l'horloge

Action mécanique des corps ayant perdu leur orbite. Cruauté de l'espace plat où résonne la cruauté des horloges. Leurs injonctions à se presser, à se haïr. Il faut tout arrêter, tout.


### Persévérer

La valeur de ce que nous faisons, nul n'est en capacité de la déterminer objectivement : le génie est celui qui s'efforce, persévère jusqu'à l'abomination, jusqu'à la nullité sociale la plus absolue, la plus communément admise --- saurons-nous persévérer jusqu'à l'abjection, les extrémités paralysantes de la honte, jusqu'à la métamorphose ?


### Béatitude

Torsion des évidences les plus élémentaires. Difficulté à vivre aimer travailler. Rien de plus suspect que la béatitude.


### Poids mort

L'écrivant est plein de ce qu'il ne peut pas dire, ce qui le rend lourd aux yeux du monde.


### Finalité

C'est qu'il doit bien y avoir l'illusion à peine camouflée d'une résolution de toutes nos petites problématiques logistiques, ou leur éviction brusque, ou leur lente dissolution, dans un jeu de tensions internes qui se maintient tout seul.


### Forage

Forer la langue en quête du juste pli.


### Sacerdoce

De l'inutilité de la joie d'écrire. Un sacerdoce. Mais notre église chante en brûlant.


### Communauté

De toute éternité l'édifice du sens est précaire : il se coltine tout à la fois l'infinie pluralité de nos expériences de la réalité, leurs devenirs foisonnants, et l'irréductible altérité qui rend si difficile et si riche la mutualisation de nos expériences.


### Passage

Ce qui a changé, avec le stade tardif du capitalisme, celui du spectacle totalitaire de la marchandise et de nos adhésions implicites (inconscientes) à ce dernier, c'est le passage de la précarité du sens à son écrasement méthodique sous l'effet d'une rationalité purement instrumentale.


### Obscurité native

Si nous voulons de nouveau rencontrer la nuit, il nous faudra devancer l'ordre des significations.


### Adieu Lukács

Redonner une valeur autonome à la parole improductive et donc génératrice de mondes.


### Le Bruit & la Fureur

Couleurs sons odeurs etc. --- régression en deçà de toute histoire --- soustraction plutôt qu'annihilation.


### Ascension

La grande simplicité suppose travail et aridité : 5 heures de marche *a minima* pour parvenir au sommet du mont Psiloritis.


### Carrière

Je voudrais apprendre à respirer les arbres.

## Document 3

Où se situent les conditions de la Révolution ? Le surréalisme n'a cessé de se rapprocher de la réponse communiste à cette question. Et cela signifie : pessimisme sur toute la ligne. Mais oui, tout à fait. Méfiance à l'égard du destin de la littérature, méfiance à l'égard du destin de la liberté, méfiance à l'égard du destin de l'humanité européenne, mais surtout méfiance, méfiance et méfiance à l'égard de toute entente : entre les classes, entre les peuples, entre les individus. Et méfiance illimitée en I. G. Farben et dans le parachèvement pacifique de la Luftwaffe. Mais quoi maintenant, mais quoi ensuite ? C'est ici qu'entre dans son droit l'élément qui exige la distinction entre la comparaison et l'image. Jamais ces deux-là, comparaison et image, ne se rencontrent d'une manière aussi brutale et inconciliable que dans la politique. Organiser le pessimisme ne signifie rien d'autre, en effet, que faire ressortir la métaphore morale de la politique et découvrir dans l'espace de l'action politique l'espace de l'image intégral. Mais cet espace de l'image n'est absolument plus mesurable de manière contemplative. Si la double mission de l'intelligentsia révolutionnaire est de mettre à bas la suprématie intellectuelle de la bourgeoisie et d'établir le contact avec les masses prolétariennes, alors elle a presque entièrement échoué dans la deuxième partie de cette mission. Et pourtant très rares ont été ceux que cela a empêchés de la présenter constamment de nouveau comme si elle le pouvait et d'en appeler aux poètes, penseurs et artistes prolétariens. En vérité, il s'agit beaucoup moins de transformer l'artiste d'origine bourgeoise en maître de l'"art prolétarien" que de le faire fonctionner, fût-ce aux dépens de son efficacité artistique, en des endroits importants de cet espace d'image. Ne pourrait-on aller jusqu'à dire que l'interruption de sa "carrière artistique" représente une part essentielle de ce fonctionnement ?

</div>

<div class="texte" data-name="Cendre">

## Cendre

### Poème pour l'enfant à naître

il y a des rivières brumes dans mon ciel

des ascendances possibles

de faux héritages

la promesse d'un nouveau détour

notre garrigue

ce terrain des rencontres fortuites

d'une étincelle et de son brasier

il y a ta mère

sa force

sa passion

ses outrages

qui fut femme avant que d'être mère

et qui le resta sans cesser de t'aimer

il y a ton père

mauvais fils obstiné dans sa forge

qui n'eut jamais de bonne situation

mais fut rigoureux dans l'étude

des phénomènes de combustion rapide

il y a courage aussi

recommencer sans cesse l'espérance

quoi que j'aie pu en dire

quoi que j'aie pu écrire

la trouée du hasard

l'art des perspectives

au cœur du seul monde qui nous soit donné

fabriquer un objet

tangible

sans auteur

sans trône

sans privilège

la trace d'une âme impie

persévérante

honnête

d'un voyou en disgrâce

car souviens-toi ma fille

que j'écrivais des lettres d'amour bourrées de fautes d'orthographe

et dans le fond d'une mare de vinaigre doux

33 ans d'âge

je trouvai la beauté en embuscade

une image

<div style="margin-left: 4em">juste une image</div>

<div style="margin-left: 8em">de notre salut</div>


### Urgence obstétrique

c'est une affaire d'aspirine

Bruce Lee en est mort

si si je vous jure

une réaction catastrophique

à 32 ans --- quasiment l'âge du Christ

ce qui est peut-être le cas de ma compagne

atteinte de deux maladies auto-immunes

mais le corps médical refuse

de considérer ses angoisses

hall triste

CHRU Carémeau

je feuillette un article

"Les violences gynécologiques et obstétriques en URSS"

comment la médecine s'empare-t-elle des corps

pour les contraindre les façonner

au nom d'une conception déterminée du Savoir

et des Méthodes qui en découlent

usant de la Science

afin de manifester et soutenir

son propre édifice

MÉ-FI-AN-CE me souffle-t-on

un scepticisme se répand dans les masses

MÉ-CHA-NT-ES MA-SS-ES AC-CU-LT-UR-ÉE-S

l'apport des sciences modernes

est considérable

oui oui je n'en doute pas

et je suis vacciné

mais quel fut le geste fondateur de Descartes ?

douter radicalement de tout

il ne se contentait pas de remettre en question

quelque croyance quelque principe

il se les caillait dans un poêle

le bruit des bottes sur le pavé mouillé

c'était la guerre déjà

mais IL N'Y A PAS

D'ÉTAT D'URGENCE EN PHILOSOPHIE

René a pris son temps

tout son temps

il est urgent de prendre son temps

nous manquons de repères stratégiques

par ailleurs

il n'est peut-être plus nécessaire

de casser des machines

les antivax sont

les luddites de notre temps

nous pensons mal

toujours en termes de Progrès ou de Réaction

alors qu'il faudrait

considérer seulement

lumière & boue

la façon dont le corps des femmes

se défend

contre toute littérature

suspendre

le pouvoir qui nous assiège

en articulant

la fin aux moyens

la médecine institutionnelle

ne pense qu'en termes de fins

tous les moyens sont bons

en ce sens elle épouse

le destin du Capital

pure espérance

d'un gain imaginaire

revendiquons une médecine

sans tarification à l'acte

sans réification des corps

une libre production associée

à d'autres corps producteurs

et dire qu'en salle d'accouchement

il n'est pas toujours permis d'user de la technologie

pour mettre de la musique

moderato cantabile

mais on peut en quelques minutes

choisir d'ouvrir un ventre

avec des moyens archaïques

j'insiste

concernant la posture d'accouchement

sommes-nous libres de la choisir ?

ne vous faites pas trop d'illusions ma p'tite

sous péridurale on a pas vraiment le choix

mais pour ce qui est de l'à-côté

gardez-le au chaud

vous en discuterez avec la sage-femme

je n'ai jamais compris

l'amour de ma compagne

pour Marguerite Duras

je le comprends moins mal désormais

depuis que je fréquente

indirectement le corps médical

indirectement je lis Duras

ce qui nous menace dans la forge

c'est le pouvoir dont on s'accommode

œil hagard

de l'efficacité thérapeutique

arrogance emmaillotée

dans une blouse blanche

si peu de cœur de considération

vite vite

prééclampsie n'est pas

une partie de cartes

vite vite

monitoring

atarax

apprenez à vous détendre Madame R

vite vite

v'là cholostase gravidique

manquait plus que ça

mais

votre bébé va bien

pas de panique

et pour ce qui est de l'à-côté

gardez-le au chaud

vous en causerez avec la sage-femme


### La Dialectique de la nature

je pars

tailler dans la pierre

un chemin

qui ne mène nulle part

j'irai voir Portbou

prendre l'air loin de notre fournaise

les genêts y fleurissent et refleurissent

toujours dit-on

sur les hauteurs de Portbou

un duvet peu de matière

textile effiloché

Faulkner

c'est si novateur qu'il m'a fallu 33 ans pour m'émouvoir

quand siffle vent sur nos plaines

les grands espaces et tout l'or des conquêtes

un simple rameau dans le silence qui trépasse

l'histoire et leurs délires de marionnettistes

juste une image

essayer de la retenir

dans le silence qui trépasse

générer de l'espace

entre les mots

entre les sensations

habiter ce crépuscule

obstinément

une poignée de genêts

jetée du haut de la falaise

libérer de l'espace

accoucher foisonner vivre

enfanter laisser mourir

W.B. mon frère

si je demeure marxiste

c'est qu'il faut soutenir l'espérance

au milieu des ruines

nous n'allons nulle part

mais nous ne partons pas de rien

et concernant la destination

je veux dire le programme

il appartient à celles et ceux qui naîtront

de le repenser autrement

notre mission aujourd'hui

consiste à nettoyer les combles

faire place nette

épurer libérer

ne plus transmettre

cette somme de problématiques sémantiques

dont nos existences sont chargées

le socialisme viendra

après un bon ménage

tu seras véritablement marxiste

m'a dit un jour ma compagne

le jour où tu mettras tes lunettes

pour passer la serpillière

le jour où

tu sauras ménager ma lumière

sans t'empêcher d'écrire

le jour où

jetant le trouble sur toute métaphysique

tu passeras maître

dans le récurage des sanitaires

par contre (a-t-elle ajouté)

je te préviens

il est très difficile de lire

la *Dialectique de la nature* d'Engels

une éponge à la main

## Document 4

Afin de récurer convenablement les sanitaires : toujours commencer par le haut. Puis descendre méthodiquement vers le fond de la cuvette. Ne pas craindre de s'y noyer. Muni d'une éponge dédiée et de vinaigre blanc, frotter circulairement. Si les éponges viennent à manquer, user de la *Dialectique de la nature* d'Engels, et notamment des pages où il est question d'une "dialectique à l'œuvre *dans* la nature". C'est seulement ainsi que nous formerons des révolutionnaires conséquents. Afin d'affiner votre formation, n'hésitez pas à faire irruption au sein de l'Institution : il est urgent de tordre le cou d'Althusser et de son marxisme scientifique servi durant tant d'années sur un plateau ("la lutte des classes dans la Théorie") par quelques employés béarnais de la cantine de l'ENS. Raison pour laquelle nous nous tapons des décennies de bourdieuseries. Renverser le plateau. Prendre soin de ramasser vous-mêmes ces éléments d'une épistémologie éclatée dont nous ferons usage plus tard : "Feuerbach prend comme point de départ le fait de l'autoaliénation religieuse, du redoublement du monde dans un monde religieux et un monde séculier. Son travail consiste à résoudre le monde religieux en sa base séculière. Mais que la base séculière se détache d'elle-même et se fixe dans les nuages en un royaume autonome n'est à expliquer que par l'autodéchirement et l'autocontradiction de cette base séculière. Celle-ci même doit donc en elle-même être tant comprise dans sa contradiction que révolutionnée pratiquement. Ainsi dès lors, par exemple, que la famille terrestre est découverte comme le secret de la sainte famille, la première elle-même doit désormais être anéantie théoriquement et pratiquement."

</div>

<div class="texte" data-name="Notes sur l’amour">


## Notes sur l'amour

Je me souviens de ce mandarinier dégoté sur un marché de Naples.

<div class="asterisme">&#35;</div>

Après l'avoir trimballé dans un train jusqu'à Marseille, j'étais fier de te le présenter sur le parvis de la gare Saint-Charles.

<div class="asterisme">&#35;</div>

Aujourd'hui sur notre balcon, il offre ses longues feuilles aux passants.

<div class="asterisme">&#35;</div>

Ceci n'est pas une histoire aimable, mais une histoire d'obstination.

<div class="asterisme">&#35;</div>

La tyrannie du bien-être nous renvoie dans la gueule l'image de notre faillite.

<div class="asterisme">&#35;</div>

Représentation assénée, statique, d'un bonheur qui nous répugne.

<div class="asterisme">&#35;</div>

L'hédonisme marchand purge nos expériences de ses complexités, de ses contradictions, de ses richesses impromptues, de tout ce qui en constitue le sens et la beauté.

<div class="asterisme">&#35;</div>

*Fugace beauté* --- non dans le bâti, marbre des splendeurs impériales, auguste déclin, etc.

<div class="asterisme">&#35;</div>

Dans l'émeute de détails sont nos tendresses mal dégrossies.

<div class="asterisme">&#35;</div>

Ce qui est valable pour la peinture l'est pour la vie domestique, mais non domestiqués, j'insiste, sont nos amours dysfonctionnels.

<div class="asterisme">&#35;</div>

*Ataraxie* : nivellement de la tension nerveuse, degré zéro de l'activité psychique, nirvana, tiédeur calme et implacable.

<div class="asterisme">&#35;</div>

Chez nous, ça flambe.

<div class="asterisme">&#35;</div>

Je ne crois pas que le culte du bien-être servira à nous détendre.

<div class="asterisme">&#35;</div>

Accepter que quelque chose nous échappe.

<div class="asterisme">&#35;</div>

Convaincu également que l'acceptation d'une certaine dose de désespoir soit juste.

<div class="asterisme">&#35;</div>

*Vitalité désespérée* : relation aux autres, à soi, au monde, qui s'affranchit de l'illusion du sens et de son marketing.

<div class="asterisme">&#35;</div>

"L'authenticité est la mort." C'est Theodor W. Adorno qui le dit, dans son bien nommé *Jargon de l'authenticité*.

<div class="asterisme">&#35;</div>

*Authenticité* : je déteste ce mot.

<div class="asterisme">&#35;</div>

Et si je te retrouvais nu, / mon amour, qu'en dirais-tu / jetant mes dernières parures théoriques à la rue / il est possible que cela serve à un passant / qu'en savons-nous / les concepts aussi se recyclent.

<div class="asterisme">&#35;</div>

L'autre est celui que je n'englobe pas dans le bien que je lui fais, m'a dit un jour un rabbin.

<div class="asterisme">&#35;</div>

Mais l'autre ce n'est pas l'enfer, l'autre c'est toi mon amour.

<div class="asterisme">&#35;</div>

En politique, comme en amour, les avant-gardes doivent demeurer intempestives.

<div class="asterisme">&#35;</div>

C'est un communisme qui prendrait acte de l'irréductible singularité des êtres, de leur histoire, de leurs limites.

<div class="asterisme">&#35;</div>

L'amour en constitue l'expérimentation la plus périlleuse.

<div class="asterisme">&#35;</div>

L'égalité en est évidemment la condition, mais celle-ci n'est ni formelle ni quantifiable : c'est une égalité des différences.

<div class="asterisme">&#35;</div>

Accepter cette distance qui met en jeu deux sujets différenciés dans un espace commun.

<div class="asterisme">&#35;</div>

Et bien que cet espace soit un désert et bien que ce désert soit le plus aride et le plus douloureux de tous les déserts du monde.

<div class="asterisme">&#35;</div>

Nous échouons en amour, c'est inévitable.

<div class="asterisme">&#35;</div>

Le bonheur conjugal est un mystère. Sa mise en scène est un mensonge.

<div class="asterisme">&#35;</div>

Remplir sa besace de mauvais romans et autres babioles : culte de l'état civil, investissements locatifs, rêves de voyage.

<div class="asterisme">&#35;</div>

Remplissage qui nous atrophie le verbe et la caresse.

<div class="asterisme">&#35;</div>

Nos capacités à être réellement, sincèrement, désespérément libres.

<div class="asterisme">&#35;</div>

Le monde serait infiniment plus doux si la douceur n'était pas une prescription médicale ou publicitaire.

<div class="asterisme">&#35;</div>

Je voudrais te retrouver nu, disais-je, / ici et donc ailleurs / nu / si simplement nu / si tragiquement nu / nu et désarmé / dans la juste présence.

<div class="asterisme">&#35;</div>

*Métaphysique du gain* : vitrine de corps larmoyants, obscènes.

<div class="asterisme">&#35;</div>

C'est ici que cela se joue, et non dans le culte des *sens retrouvés*, sensualisme bas de plafond.

<div class="asterisme">&#35;</div>

Nous assistons à la prolifération de nouvelles techniques thérapeutiques *axées sur le corps car on n'en peut plus d'intellectualiser*, leurs méthodes visant une efficacité thérapeutique *à tout prix*, et cette promesse enfin d'une *résolution de nos dysfonctionnalités*.

<div class="asterisme">&#35;</div>

Et si nos dysfonctionnalités étaient les dernières expressions inconscientes de quelque chose qui continue de résister en nous ?

<div class="asterisme">&#35;</div>

Contre cette grammaire du réel fondée sur une valeur extraite de l'exploitation, non plus seulement des autres, altérités réelles ou supposées, mais de nous-mêmes.

<div class="asterisme">&#35;</div>

La culture du pouvoir se pâme de tolérance jusqu'à l'indigestion --- et se fonde conjointement sur l'éradication de tout ce qui lui échappe.

<div class="asterisme">&#35;</div>

Désirer plus que jamais la véritable solitude, maniaque, salvatrice, mystique.

<div class="asterisme">&#35;</div>

Et me souvenir du mandarinier : rester.

## Document 5

La société bourgeoise insiste constamment sur les efforts de la volonté ; seul l'amour doit être involontaire, pure immédiateté du sentiment. Dans son aspiration à cette immédiateté qui signifie que l'on est dispensé de travailler, l'idée bourgeoise de l'amour transcende la société bourgeoise. Mais en érigeant la vérité directement au sein de la non-vérité généralisée, elle pervertit la première et lui fait rejoindre la seconde. Si l'amour doit représenter dans la société la perspective d'une société meilleure, il ne peut le faire comme une simple enclave pacifique, mais par une opposition consciente. Celle-ci exige précisément ce moment de volonté que lui interdisent les bourgeois pour qui l'amour ne sera jamais suffisamment naturel. Aimer, c'est être capable de ne pas laisser dépérir l'immédiateté sous la pression omniprésente de la médiation, de l'économie et, dans cette fidélité, l'amour se médiatise lui-même, il devient contre-pression opiniâtre. Celui qui a la force de s'attacher fermement à l'amour aime vraiment. L'amour qui, sous l'apparence de la spontanéité irréfléchie et fière de sa prétendue sincérité, s'abandonne entièrement à ce qu'il prend pour la voix du cœur, s'enfuyant dès qu'il ne croit plus entendre cette voix, est précisément --- dans sa souveraine indépendance --- l'instrument de la société. Passif sans le savoir, il enregistre les chiffres qui sortent à la roulette des intérêts. En trahissant l'être aimé, il se trahit lui-même. La fidélité ordonnée par la société est le moyen même de n'être pas libre, mais seule la fidélité permet à la liberté de se rebeller contre les ordres de la société.

</div>

<div class="texte" data-name="Le livre de Nina">


## Le livre de Nina

<div class="centre italique">

quand les anges de l'Islam

quand les anges de Paul Klee

daigneront se rencontrer

alors nous te retrouverons

<div class="dedicace">&nbsp;</div>

musique si neuve

qu'on ne peut aujourd'hui

l'imaginer sur terre

<div class="dedicace">&nbsp;</div>

en attendant il faut vivre

soulever jour après jour

pierre scellée de notre perte

<div class="dedicace">&nbsp;</div>

épave transie de notre amour

au fond d'une calanque marseillaise

<div class="dedicace">&nbsp;</div>

la nuit du monde regorge

de lumières insoupçonnées

<div class="dedicace">&nbsp;</div>

j'ose croire que c'est toi

qui allumas ces clartés

le long de nos rues mortes

<div class="dedicace">&nbsp;</div>

nos utopies sont lourdes

et n'en seront que plus réalistes

et n'en seront que plus concrètes

<div class="dedicace">&nbsp;</div>

"il faut que le ciel s'améliore"

t'ai-je entendue dire dans un rêve

sache que nous y travaillons

</div>

## Document 6

Et c'est pourquoi ils n'habitent pas le Christ, ces acharnés du cœur qui toujours le fabriquent à nouveau et vivent de dresser des croix, penchées ou battues par les vents. Ils l'ont sur la conscience, cette bousculade, ce piétinement sur la place surpeuplée, c'est par leur faute que la marche ne continue pas dans la direction indiquée par les bras de la croix. Le christianisme, ils en ont fait un métier, une occupation bourgeoise, un travail à façon, un étang qu'on vide et qu'on remplit sans fin. Ils souillent leurs propres eaux et doivent sans cesse les renouveler. Pas un instant ils ne relâchent leur zèle à abîmer et dégrader l'Ici-Bas, qui ne devrait pourtant nous inspirer que joie et confiance, --- et ainsi chaque jour davantage ils livrent la terre à ceux qui sont prêts à en tirer au moins un bénéfice temporel et un profit rapide --- cette terre ratée, cette terre suspecte qui, au fond, ne mérite pas mieux... Quelle folie de nous détourner vers un au-delà, alors que nous sommes ici pressés de toutes parts de tâches et d'attentes et d'avenirs ! Quelle imposture de confisquer les images de l'extase d'Ici-Bas pour les marchander au ciel, derrière notre dos ! Oh, il serait grand temps que cette terre spoliée récupère tous les emprunts qu'on a faits à sa béatitude pour en parer un au-delà futur. Je ne peux m'empêcher de penser que c'est faire injure à Dieu de ne pas voir, dans ce qui nous est accordé et confié ici-bas, un bonheur capable de combler pleinement nos sens, jusqu'à ras bord --- pour autant simplement que nous en faisions juste usage. Le juste usage, voilà l'important. Prendre bien en main l'Ici-Bas, avec un cœur plein d'amour et d'étonnement, comme notre unique bien, provisoirement : voilà, pour le dire familièrement, le grand mode d'emploi de Dieu. Celui que saint François d'Assise a voulu transcrire dans son cantique au soleil : car sur son lit de mort, le soleil lui apparut plus magnifique que la croix, qui n'avait de raison d'être là que pour le montrer. Mais ce que l'on nomme l'Église, avait déjà entre temps enflé en un tel tumulte de voix que, couvert de toutes parts, le chant du mourant ne fut perçu que de quelques simples moines, et acclamé à l'infini par le paysage riant de sa vallée.

</div>

<div class="texte" data-name="Sentier">


## Sentier

<div class="centre">

L'étoile n'est pas tou-

jours l'étoile qui élit car

faible intensité.

<div class="asterisme">&ast;</div>

Ne pas en vouloir

aux indifférents qui gloussent

pour ne pas pleurer.

<div class="dedicace">&nbsp;</div>

Nous aussi amie rions

les anges nous cuisinent

un foie pour l'hiver.

<div class="asterisme">&ast;</div>

Se remplir de vin

ne console guère cœur qui

hurle en son désert.

<div class="asterisme">&ast;</div>

Ferme les yeux pour

voir le sentier des braves qui

luit sous la bruyère.

</div>

## Document 7

On dirait des ruisseaux de féerie, de la lumière figée, des coulées de soleil. Le sol tremble, et, devant moi, par un trou gros comme la tête d'un homme, s'échappe avec violence un immense jet de flamme et de vapeur, tandis qu'on voit s'épandre des lèvres de ce trou le soufre liquide, doré par le feu. Il forme, autour de cette source fantastique, un lac jaune bien vite durci. Plus loin, d'autres crevasses crachent aussi des vapeurs blanches qui montent lourdement dans l'air bleu. J'avance par crainte sur la cendre chaude et la lave jusqu'au bord du cratère. Rien de plus surprenant ne peut frapper l'œil humain. Au fond de cette cuve immense, appelée "la Fossa", large de cinq cents mètres et profonde de deux cents mètres environ, une dizaine de fissures géantes et de vastes trous ronds vomissent du feu, de la fumée et du souffre, avec un bruit formidable de chaudières. On descend, le long des parois de cet abîme, et on se promène jusqu'au bord des bouches furieuses du volcan. Tout est jaune autour de moi, sous mes pieds et sur moi, d'un jaune aveuglant, d'un jaune affolant. Tout est jaune : le sol, les hautes murailles et le ciel lui-même. Le soleil jaune verse dans ce gouffre mugissant sa lumière ardente, que la chaleur de cette cuve de soufre rend douloureuse comme une brûlure. Et l'on voit bouillir le liquide jaune qui coule, on voit fleurir d'étranges cristaux, mousser des acides éclatants et bizarres au bord des lèvres rouges des foyers.

</div>

<div class="texte texte--aleatoire" data-name="Phrases A">

<div class="a-droite dedicace">

*Pardonnez-moi, ma très chère mère, s'il ne m'est pas donné de me faire entièrement comprendre de vous.*

--- Friedrich Hölderlin

</div>

### A

Tout ce que j'ai toujours désiré c'est un coin de terre paisible qui m'offrirait de temps à autre une petite saloperie de nouveauté pour m'aiguiser la seule dent qui me reste au milieu du visage cette nouveauté pourrait être modestement un os qui n'aurait jamais été remâché par l'homme ou un petit caillou en forme de fleur ou une syntaxe un peu différente à la manière de la littérature qui s'ignore mais se défend en allumant un cierge païen au cœur d'une église livide et blanche et orpheline ou bien encore un simple murmure du vent mais dans le genre saccadé et caustique qu'importe finalement la nature de cette nouveauté tant qu'on me fout la paix et qu'on me permet de jouir d'un petit coin de terre paisible sans école sans déclaration périodique sans pissenlit sans rien qui me chagrine et me remplisse car je ne veux surtout pas me remplir surtout pas je veux me vider et me vider jusqu'à la moelle j'y tiens plus que tout j'y tiens amoureusement c'est une intuition éthique n'entendez là-dedans rien de sexuel je veux simplement me vider me vider entièrement n'être plus rien qu'un élément discret dans le tapage prophétique d'une danse rien de bien éloquent rien de bien solide rien qu'une feuille desséchée qui a passé l'hiver en se craquelant dangereusement et dont les craquelures sont autant de déchéances tranquilles je veux me vider de tout sentiment articulé de toute inclination au discours me vider au point de n'être plus qu'une forme géométrique un quadrilatère peut-être mais je n'en suis pas certain j'ai aussi le goût des lignes droites sans compromis jouissant de l'ordre austère d'une difficile simplicité rien à négocier avec la beauté sa possibilité peut-être cette possibilité qui m'interpelle et me révoque il est grand temps de casser des assiettes de faire un beau ménage au sein de notre écologie du sens un recyclage méthodique de nos écarts de nos grossièretés de nos outrages une concentration de gaz féeriques et intenses ne laissant guère indemne l'hypothèse farfelue d'une existence solide se débattant dans un jeu de miroirs sordide déniaiser l'errance briser les miroirs amplificateurs de culpabilité et de romance déniaiser l'errance au cœur de ce petit coin de terre paisible où nos rivières se sont révélées plombées c'est le plomb de l'absence ou bien celui de vieilles raffineries un reste de cyanure un rien de complaisance dont l'extraordinaire contagion a saccagé notre butin d'hiver hors de question pourtant de se vautrer dans la purée de tomates nous ne faisons que passer certes mais alors pourquoi employer la vacance la prophétie n'a plus cours s'est effondrée plombée la vacance et l'aube idiote je l'ai longtemps cherchée longtemps convoquée longtemps espérée jusqu'au bout des nerfs jusqu'au bout de nos révolutions brisées avec une gourmandise à peine dissimulée et un amour terroriste de l'outrance mais en l'absence d'une confirmation secrète d'un signe même involontaire et au prix d'un fracas ayant tant remué la papauté nous nous trouvons livrés au culte du remplissage et seule la mort nous a quelquefois démentis la mort qui est une réaffirmation contrariée de la vacance les esprits étroits préfèrent la nommer liberté ce qui est un manque de raffinement je préfère me pendre à l'arbre touffu de l'enfance tout est médiocre et fade loin de l'enfance qui s'est barricadée dans un édifice de verre sculpture plate et garnie de mauvaises odeurs nous ne sommes plus vraiment là où l'enfance se dérobe rien n'est écrit tout devance la conscience de notre dispersion pourtant il n'est plus question de nous appartenir nos larmes condensent résidus d'orage dans paume de main analphabète et cassante condensent outrance en la pressant comme citron de Sorrente au point de n'en conserver que la souche cellulaire primordiale dont nous sommes convaincus que la vocation est une lumière et que cette lumière se confond avec le miracle de la naissance loin des palabres tristes sires osons le goût retrouvé des massacres la mécanique est irrémédiablement atteinte le diable s'est figuré que nous avions des tresses d'argile qu'il pouvait à sa guise s'amuser à broyer l'errance qui se débat comme un mirage avec tous ces avions et ce silence tous les mirages de l'enfance qui se débattent et tremblent dans un style petit chiot austère nos détresses en quête d'une méthode efficace comment passer l'hiver comment nous réparer après la mort d'un enfant comment continuer à allumer des cierges même païens à l'aune de cette trahison profonde de ce bain rassis d'écume et de brume en un mot d'emphase quand il n'y a rien à espérer rien de neuf quand le coq de France ébruite sa plainte quand le marteau et sa faucille sont devenus si lourds il n'y a plus rien à espérer rien de neuf sous le ciel de France où les refrains se font latents comme des rêves d'ivrogne je leur préfère le rire truculent des anges quitte à sacrifier ma substance à n'être plus qu'un homme qui se noie dans la boue de sa propre langue dans le ventre d'un siècle qui exténue l'enfance.


</div>
<div class="texte texte--aleatoire" data-name="Phrases B">

### B

J'élève des parpaings en guise de murs afin d'éviter le supplice d'une visite d'un flic errant d'un père ou toute intrusion de l'aube dans mon fouillis ordonné et précaire sans risquer l'azur une seconde fois ni les grands chambardements de l'art neuf ayant perdu le goût et la manière de fabriquer des ciels avec de la poussière il faudra nous contenter des voûtes consacrer son temps à l'épuration méthodique de toute trace d'effusion tenace l'effusion dont on ne sait jamais si elle se lamente ou si elle chante nous n'avons pas de théorie succulente nous permettant de trancher dans le lard un simple récépissé de nos audaces passées peu flatteur et nauséeux encombrant comme cet oiseau méchant qui vomit ses tripes en hymne lancinant il est temps peut-être de déjuger toute idée de profondeur et de dévoilement la profondeur est très peu fonctionnelle le dévoilement ne dévoile rien il distingue c'est se répandre en coquetterie et si peu de muscle d'un lyrisme épuisant le nerf et l'insouciance quand il faudrait simplement travailler la surface en s'inspirant de tâches ménagères concrètes les plus basiques sinon peu d'espace pour ordonner notre fouillis un bon ménage c'est le moyen le plus honorable de se concentrer sur une chose à la fois et tant pis si on me confond sur ce point puisque je n'ai pas d'amis parmi les scribouilleurs de ceux qui martèlent l'idée pimpante jusqu'à la nausée quitte à épingler Descartes sur un vieux mur en liège rongé par des punaises pas du tout certain que René en sorte indemne d'autant que les étouffe-misères ne savent ni danser ni s'occuper d'un potager et que la chair d'un homme est une matière précieuse et périssable que son chapeau n'y peut rien bien qu'un brin démodé il réchauffe la tête et ça c'est très fonctionnel ajoutez-y le hennissement d'un âne cabossé tout seul dans son enclos et vous aurez le petit précis de modernité qui manque au margoulin des belles lettres c'est une moindre peine en entendant la pluie tomber de se sentir désarmé si bottine cryptobismarkienne glisse aisée sur le pavé mouillé cogito étalant sa résine sous nos voûtes pour empêcher la pluie de tomber quelle ironie la pluie imaginez la gueule des anges s'ils apprenaient que nous négligeons la pluie qui donc s'occupera de fabriquer des ciels tout neufs si on néglige la pluie impossible de les désunir azur et pluie sont comme chien et chat la relégation de l'un entraîne inévitablement la relégation de l'autre pourtant il existe beaucoup d'hommes qui usent de reniement en matière de pluie et je ne leur garantis pas pour autant un azur de première main je suis dur avec les scribouilleurs de notre temps admettons qu'il en faut du courage pour pisser droit en contemplant les étoiles sans négliger la goutte d'effusion démocratique permettant aux vainqueurs de retomber sur leur selle dans la cohue inerte d'électeurs n'ayant pas encore perdu le sens mélodique ni celui du mérite et moi qui ne peux rien revendiquer je chante faux me martelèrent si souvent mes professeurs quand j'osai réclamer une prodigieuse extension de la récréation dire pourtant que j'étais le seul à me vautrer dans les luttes et les rêves au point de compromettre mon obsession de la métrique qui s'en trouvait ainsi contentée et le contentement ça affame le cœur ô combien furent longues et laborieuses mes années d'arrachement au sein moelleux de la République c'est la raison pour laquelle je n'ai plus qu'une dent au milieu du visage raison pour laquelle j'élève des parpaings aux quatre coins de mon petit jardin raison que me dicte mon angoisse d'être démasqué comme au temps de l'adolescence comme au temps des supplices comme au temps des railleries qui me pillèrent le rire et l'insolence alors j'élève des parpaings contre les moqueries j'élève des parpaings afin de faciliter le règne des métamorphoses silencieuses je proclame solennellement la commune des taupes quitte à sacrifier ma lumière et à ronger l'os de mes jours je préfère me terrer loin des visages bouffis loin des académies à une distance salutaire des colliers de dents et d'ivoire loin des déballages obscènes et des revendications de l'aube loin des Duchesses et des souliers vernis rouges et noirs qu'importe l'aube a mauvaise presse pourtant il n'est pas certain que le bourgeois régale autant que les souliers rouges ou noirs des duchesses qui avaient le mérite de briller dans la nuit de l'Histoire alors j'hésite à poser un toit sur mes quatre rangées de parpaings à y établir une sorte de cube afin de m'empêcher de divaguer en regardant les étoiles afin de m'empêcher de me souvenir de son visage sa beauté me lacère quand je la fixe à ma nuit non je n'oublierai pas son visage l'enfance est une rivière où l'or a disparu depuis deux millénaires mais s'obstine à chanter de plomb et d'or sont nos rivières sur les rives desquelles je bâtirai d'autres ciels en carton-pâte muni de colle d'amande et de petits crayons évitant les inscriptions lourdes et les formules péremptoires nous accueillerons gentiment la lune et tous les animaux qui le souhaitent pour te rendre la vie ma petite chérie pour te rendre la mort moins rude et ton absence moins folle j'élèverai des parpaings pour couver ta mémoire pour éviter que le bruit des bottes et le crissement des uniformes ne t'effraient j'élèverai des parpaings comme des barricades contre le sens du vent et tous les arts déclamatoires fades et mielleux disparaîtront en un éclair et nous serons réconciliés avec les millions d'étoiles qui gesticulent au-dessus de nos têtes et nous serons foyer et nuit et aube peut-être.


</div>
<div class="texte" data-name="Phrases C">

### C

Orphelin de lunes nourricières et de champs de métrique j'ai brûlé l'écriture et l'ossature des rêves j'ai brûlé forêts du monde et leurs hivers j'ai brûlé ginestra des moines inoubliable ascèse j'ai brûlé étés en leur désert un cri sans mégaphone le chant lancinant des feuilles mortes exécutant à l'aube le sens et le frisson j'ai brûlé Hugo et Aragon j'ai brûlé Grande Poésie Nationale j'ai brûlé ponctuation fous-t'en Depestre fous-t'en tout ne finit pas dans une indigestion de roses et de chansons j'ai rêvé d'un art sec et minutieux aride et d'une rigole pour me noyer sauvage et me loger timide dans le soupir d'un lieu j'ai rêvé de paix de silence d'irréductible nuit j'ai rêvé d'une langue morte et insulaire Ulysses back on the path j'ai rêvé de Dublin des baisers de bravoure échangés au bord d'un vieux canal d'une friche ouverte aux désespérés du petit voyou de 7 ans cherchant la mer et ses paquebots loin de l'école on dit que les cartographies se dérobent j'ai prié le petit camarade de me sourire en partant pour me donner la force de continuer et j'ai rêvé une fois encore de pirates d'hommes-chiens et de lézards géants j'ai rêvé d'armada douce de guanches intrépides et méchants j'ai rêvé de Kiki Colomb agonisant dans le cratère du Teide à 3 500 mètres d'altitude et de ce vent sablonneux infiltrant les bronches d'un autre génois marchand d'art de sa profession et fossoyeur de littérature j'ai nommé André Breton j'ai rêvé d'oranges très amères et de citrons empoisonnés de sable noir et de belles contagions j'ai rêvé d'un océan au cœur foisonnant de Jersey de Cuba de vierge noire et de mezcalería j'ai rêvé de Mexique et de grandes fatigues j'ai rêvé de guingois et de lignes droites j'ai rêvé de lances en peaux de mouton et de canons inoffensifs j'ai rêvé d'abolir de long en large le calendrier grégorien le style et la syntaxe et le sourire oblique des scribouilleurs de l'Histoire j'ai rêvé de plonger tout ce beau monde dans ma rigole et tout recommencera alors puisqu'il faut bien que tout recommence un jour dans la métaphore chauve d'une rigole et non plus dans l'océan foisonnant que je ne salue plus que par habitude ou par compassion que je continue à fréquenter tout de même mais du bout des larmes comme un trapéziste obèse ayant rompu la corde de son supplice et je fomente ainsi dans ma solitude maniaque des insurrections enfantines traçant sur le sable le nom d'un ange que les oiseaux picorent sans concession car le mutisme des océans car la machinerie des vagues quand tous les vieux pillages de l'ordre et de la raison mais ça je le disais déjà en l'an 1492 vénérant alors d'autres califats d'autres architectures d'autres prophéties et bien que Jorge nous réconcilie un brin avec l'église j'en veux à ma croix de tenir droite plantée là dans le sable blanc alors que le nom de mon ange se fragmente dans le sable alors que le nom de mon ange est Nina Samuelle Nour et qu'elle manque à ses parents comme un cœur à ses artères mais il faut bien que tout recommence dit-on alors j'arrache sa gueule de troubadour à l'insouciance humaine et jette dans nos rigoles l'ultime assaut notre guerre ne fera pas de victimes nouvelles nous parlons depuis la nuit totale depuis ce coin de terre paisible où la douleur chante nous parlons depuis les ruines jouxtant notre rigole creuse rigole de ton exil ardent épuise nerf du souvenir et cesse de t'offrir en pâture aux oiseaux cruels qui nous épient regarde cette gentille petite flaque de boue avec un bâton je fais des cercles dans la boue je fais de petits cercles dans la boue et j'oublie tout j'oublie vieil océan j'oublie complicité des tempêtes j'oublie douleur et cendre je t'en foutrai des hommages moi je fais des cercles dans la boue de petits cercles dans la boue avec un bâton je fais des cercles à n'en plus finir de petits cercles dans la nuit avec un bâton je fais des cercles dans la boue de petits cercles dans la boue et je jette un genêt dans la boue un genêt subtilisé en hâte au bord d'une autoroute un genêt dont les pétales se désunissent dans la boue dans cette rigole creusée avec amour et dévotion afin de permettre l'irrigation de ces tomates qui ne verront jamais l'été avec un bâton je fais des cercles dans la boue de petits cercles dans la boue et les pétales de mon genêt se désunissent encore c'est un genêt d'ici et non d'ailleurs un genêt subtilisé en hâte au bord d'une autoroute ô je sais bien qu'il existe une grande variété de genêts mais ne m'en voulez pas si ce n'est pas même un genêt de Sicile pas même un genêt de Collioure un simple genêt d'ici un simple genêt qui n'a vraiment rien d'épique un simple genêt breton ou cornique un simple genêt avec ses pétales qui se désunissent toujours avec ses pétales qui dansent en se séparant avec ses pétales qui dansent dans la nuit immense alors je cesse de tracer des cercles dans la boue je cesse de tourner en rond je regarde les petits pétales de mon genêt qui dansent dans la nuit ses petits pétales qui se disent adieu dans la nuit ses petits pétales qui ne se reverront jamais plus ses petits pétales jaunes je crois comme le sont les pétales de tous les genêts du monde jaunes et désunis dans la nuit immense.

</div>
<div class="texte" data-name="Phrases D">

### D

J'ai mangé tous mes livres l'Histoire un accident terrible tant de croisades auxquelles je n'ai pu prendre part je veux dire en dehors de toute considération mettant en jeu mon fameux libre arbitre en somme que je le veuille ou ne le veuille pas je n'étais qu'un enfant et ma volonté pudique balbutiant quelque ignominie ou fallacieuse sentence bref c'est une infirmité fondamentale et honteuse m'imposant avant toute autre considération de vivre reclus sous un amas de textile et de mousse mais il faut être bien triste dit-on pour susciter Carthage et il est vrai que longtemps je mélangeais vents fleuves massifs longtemps je vivais prisonnier de géographies insensées et sublimes et on comprendra très bien qu'à ce titre je privilégiasse à la face calcinée des Romains les petits feux jaunes verts et rouges de la résistance punique établissant chaque soir de drôles de remparts autour de mon lit je me fabriquais ainsi un abri contre Rome ou ces hordes de mercenaires me pourchassant au seuil du sommeil mon enfance fut une éternelle guerre de position sans audaces conquérantes sans prouesses ni facéties mais je le confesse aisément aujourd'hui je tirais alors un orgueil certain de mon incapacité à me vautrer dans l'arène sans pour autant fléchir devant toutes ces batailles perdues d'avance non je n'ai jamais reculé au moment fatidique où il s'agissait d'armer nos impuissances et j'ai tenu Carthage à la force du rêve et j'ai tenu Carthage pour toi que j'ai toujours connue que j'ai toujours aimée même en un temps où Carthage n'existait pas oui j'ai tenu Carthage pour toi et chaque aube était une victoire de la délicatesse orientale sur la brutalité du nouveau monde chaque matin je pouvais m'exclamer que Carthage était sauve que Salammbô vivrait me repliant ainsi sur mon rêve songeant que j'étais sans doute le fils bâtard d'Hamilcar et que terré dans l'ombre de ma sœur je n'avais rien à revendiquer qui n'ait attrait au mystère des nuits de Mégara quand tapi sous un fauteuil de paille j'écoutais Salammbô chanter pour Tanit et me laissais envelopper de ses élans passionnés en répétant tout bas ses dithyrambes en me les répétant au point de m'évanouir avec eux dans la nuit profonde et argentée de m'oublier entièrement en eux jusqu'au moment où se tournant vers moi ma sœur qui jusqu'ici avait feint de m'ignorer m'invitait de sa voix chaude à regagner ma chambre ce moment d'exaltation sereine étant néanmoins concomitant d'un supplice dans un lieu moins réel et plus triste dans ma chambre parisienne où mon père entrait rudement et me hurlait qu'il fallait se lever s'habiller partir à l'école cet instant aux deux facettes condensait en lui-même l'opposition violente du rêve et de la fade réalité cet instant dont l'image bizarre et contrastée réunissait deux pôles opposés l'un magnifiant la lumière des nuits de Mégara l'autre incarnant l'autorité brutale d'un père qui de sa voix nerveuse et angoissée m'ordonnait de me lever en hâte de me vautrer ainsi dans l'arène moi l'esthète moi le cancre moi le petit frère de Salammbô j'étais donc tenu d'abdiquer de me coltiner encore une méchante journée d'école quand toutes les leçons d'histoire et de géographie ne m'ont jamais parlé de Carthage ne m'ont jamais fait connaître le mystère de ses nuits la vaillance de ses soldats le raffinement de ses festins ou si mal en brandissant des dates et des conflits des catégories infâmes la carte n'est pas le territoire et Salammbô dans vos livres n'apparaît nulle part enfin ma révolte prit un tournant nouveau un matin où ayant traîné ma carcasse à l'abattoir un petit matin blême succédant à une nuit valeureuse et brillante pendant un cours d'histoire durant lequel les contorsions de l'ordre et du savoir me firent enrager plus qu'il ne m'est permis aujourd'hui de l'envisager je me levai d'un bond et courus vers la porte afin de me sauver d'échapper ainsi à cette inévitable agonie que me promettait la perspective de deux longues heures de cours moi le petit frère de Salammbô moi l'esthète moi le cancre car je le confesse ici j'ai toujours confondu ma main droite avec mon pied gauche et c'est avec le pied gauche que j'écris et c'est avec la main droite que je pense à l'école jamais je n'ai posé de mots sur une feuille qui ne me coûtèrent l'éclatement de mon âme la convulsion de mes instincts bien au-delà de l'ennui dont les singes s'accommodent je n'ai jamais su sacrifier Carthage aux conquêtes de Rome et j'ai veillé sur elle toutes les nuits comme un soldat sans armes ainsi que j'ai veillé sur toi quand me tenant au bord de ton lit je profitai d'un moment d'accalmie pour te lire quelques pages d'un récit de Carthage les seules qu'il me fut possible de te lire vois-tu je suis heureux que tu partes avec ces quelques pages je sais que c'est toi désormais qui veilles sur elle dans ta nuit profonde et argentée dans ta nuit jaune verte et rouge et je me surprends à espérer que veillant ainsi sur Carthage tu veilles aussi sur l'enfant que j'étais.

</div>
<div class="texte" data-name="Phrases E">

### E

Je quitte mon petit fief épique et ample mes souvenirs d'enfant afin de renouer avec un coin de terre paisible il est temps avant que l'hiver ne décline de suspendre un peu la voix du souvenir de nous fondre dans le silence des nuits bretonnes d'y goûter leur éclat pudique et l'odeur de l'iode il est temps d'y revenir autrement dans l'enfance autrement qu'à coups de hache métabolique et de vins de Carthage il est temps de plonger dans la nuit de l'enfance et de reprendre une à une les tâches auxquelles je souhaite astreindre ma vie JE LE CRIE OUI QU'IL FAUT BOULEVERSER LA SYNTAXE ce qui n'est pas seulement une question hermétique de formaliste etc. c'est une question sérieuse il s'agit de faire entrer la vie de nouveau dans le poème alors oui on est passé à côté de beaucoup de choses essentielles oui on a retenu des larmes et oui il faut QUE LES POULES AIENT DES DENTS POUR MORDRE DANS NOS NUITS on ne se satisfait aucunement de l'humble réalisme ni de la féerie béate et oublieuse des ravages du temps ni du sentimentalisme nul besoin ici d'accoler le moindre épithète ON CREUSE DES TROUS C'EST TOUT DANS LA TERRE DU LANGAGE le vide convoque un rapport nouveau le hasard souvent plus inspiré que la bonne volonté humaine sculpte des images en défonçant des portes scellées TOC TOC C'EST L'AURORE cela requiert beaucoup de force et de méthode c'est épuisant l'aurore mais la bonne fatigue c'est connu permet de relâcher un peu nos systèmes nerveux comprimés de nous détendre le muscle de libérer des espaces dans la phrase mais je m'égare revenons à nos poules qui entament ici le cycle des métamorphoses silencieuses pardon oublions nos poules et revenons à nos sœurs les oies qui ne cessent de jacter l'élevage des oies est une activité improductive ça ne sert à rien d'élever des oies si ce n'est pour leur manger le foie L'ÉLEVAGE IMPRODUCTIF EST CE QUI RESSEMBLE LE PLUS À L'ACTE D'ÉCRIRE quand ça jacte et que ça a faim on ne peut se résigner à la torpeur il faut travailler se lever tôt pour nourrir cette urgence les oies jactent comme nos phrases il est urgent de les alimenter passé 7 heures elles se lamentent il n'y a rien de plus grave qu'une phrase qui se lamente c'est comme un poumon qui siffle c'est absolument sinistre il n'y a rien de plus triste qu'une oie sous-alimentée à moins peut-être d'une oie gavée sournoisement à l'approche de Noël mais l'équilibre est précaire je veux dire en matière de phrase il faut trouver l'équilibre qui n'est pas nécessairement une harmonie mais un visage même cassé cela doit rester un visage on ne sait jamais vraiment ce que manifeste un visage ça nous échappe toujours un peu un visage jusqu'au jour où ça nous échappe complètement et alors ça devient un langage un langage qui n'a rien d'un système de signes cohérent et définitif le langage qui n'est pas même la langue mais sa subversion perpétuelle souviens-toi le visage créole de la mort tel que savent uniquement le manifester ceux qui ont renoncé à la grammaire ceux qui ont défait la syntaxe pour la reconstruire un peu autrement dans un lieu neuf C'EST DANS L'ÉCART ENTRE LE SENS ET LE SIGNE QUE SE LOGE LE POÈME et c'est sans doute affreux mais c'est ainsi à mi-chemin toujours dans le foisonnement silencieux d'une forêt cet écart suscite le vertige il est temps de s'y engouffrer vents d'est vents d'ouest le tout c'est de passer l'hiver N'ATTENDEZ SURTOUT PAS L'INSPIRATION ELLE N'EXISTE PAS une lubie ayant pour fonction d'octroyer une aura prophétique au poète et de dédouaner un peu nos paresses IL FAUT CREUSER CHAQUE MATIN DANS LA TERRE DU LANGAGE le matin pas l'après-midi pas le soir il faut commencer le plus tôt possible car au fil de la journée nous récupérons la pleine maîtrise de la syntaxe et nous perdons la mémoire involontaire de cette relation intime au miracle de la vie au mystère de la mort cet espace ouvert où rien ne se corrompt le rythme le souffle la vision IL EST URGENT DE PUISER DANS LE LAIT MATERNEL L'ENVERS DES SIGNIFICATIONS entends battre le pouls des terres négligées battu par le vent le chant des opprimés petite chapelle où chevalier san Galgano a déposé les armes se consacrant ainsi à l'étude des mystères RIDEAU MES FRÈRES IL EST TEMPS D'ORGANISER LA VIE AUTREMENT la fidélité à la lumière requiert ascèse et obstination LA MORALE EST TOUJOURS UNE RELIQUE DE LUMIÈRE quand les seuls pèlerinages utiles au monde sont ceux de l'audace et de la solitude IL EXISTE AINSI DES CHEMINS DE PURE LUMIÈRE mais ces derniers impliquent de renoncer à toute considération grammaticale à toute édification républicaine du sens LA VÉRITABLE LUMIÈRE EST UN CHEMIN NON BALISÉ n'oublie pas que les anges sont cruels ce qui n'altère en rien notre amour n'oublie pas que le silence bourru des plaines et le chant sous-jacent des oiseaux sont une offrande et jamais un symbole en eux-même IL FAUT S'OUBLIER DANS LES ONDES afin de renouer avec l'écriture à mi-chemin toujours de la matière et de l'esprit NE PAS CHOISIR MAIS SE LOGER DANS L'INTERSTICE à mi-chemin toujours de la carte et du territoire À MI-CHEMIN DU RÊVE ET DE L'HISTOIRE on dit que ce sont des oies qui ont sauvé Rome en jactant si fort que les romains eurent tout juste le temps d'échapper à l'assaut des celtes LE POÈME EST INFIDÈLE il poursuit son propre chemin rugueux nous ne serons sauvés par RIEN NE NOUS EST ÉTRANGER DANS LA MORT cela suffit à recommencer chaque jour à nous donner la force de nous lever à l'aube pour travailler N'OUBLIE PAS DE CREUSER TON PROPRE TOMBEAU une pierre lourde sur le marché des significations nous sommes en chemin nous sommes déjà morts nous sommes le chemin etc.

</div>
<div class="texte" data-name="Phrases F">


### F

Ça y est je tremble la mesure des lentes moissons je tremble un détour par le sentier des éphémères je tremble car je n'ai plus peur il faut s'attarder je crois sans s'appesantir buvons l'eau des sources calmes et pissons dans les ronces puisqu'il faut fleurir la pierre qui bat nos mélancolies minérales le chemin de la pierre à la fleur le chemin du cœur à la pierre à la fleur et donc la rencontre la voici la revanche de nos corps aimants désarticulés devant la mort ça y est je tremble dans un espace où la fleur où la pierre un espace où s'abolit je me rapproche du centre à mesure qu'il s'évanouit je me rapproche lentement de cet espace auquel j'ose patience je suis un être d'amour pour fleurir les pierres ont besoin de temps que la pierre me pardonne que la fleur me pardonne la pierre pour battre la rencontre la fleur pour chanter mais j'ai négligé le mystère de la rencontre mais j'ai négligé son chant à l'assaut toujours en partance vers une île impossible je n'ai su qu'être contre avant d'ouvrir les yeux sur toi qui es fleur qui es pierre qui es cendre maintenant sur tes yeux de pierre sur tes yeux de fleur sur tes yeux de cendre j'ai ouvert mon cœur sans le répandre sans le livrer à la formule espiègle d'un discours j'ai ouvert mon cœur lentement et je pose ta pierre au cœur d'un champ de fleurs je ne prétends pas connaître je ne prétends pas le nom des fleurs je les voudrais sauvages pour que la pierre se réchauffe au récit des couleurs pour que la fleur se plaise ici un champ de fleurs sauvages que j'imagine aussi beau que ces petits promontoires rocheux au-dessus de Callelongue petits promontoires fleuris je tremble car je me souviens le temps parle le langage des morts quand il s'est couché sur notre perte nous avons pris le temps tout le temps nécessaire pour te regarder partir avec le couchant et nous fûmes pierre et fleur en leur union doucement nous fûmes chant et prière lentement le couchant dicte sa loi consentir il faut simplement consentir nous aurons tout le temps de repartir à l'assaut en partance vers une île impossible nous aurons tout le temps rien pas même la guerre pas même le mugissement des révolutions rien ne justifie de ne pas prendre le temps de regarder notre fleur partir avec le couchant rien ne justifie d'oublier le chant d'oublier la pierre rien ne justifie de se presser pas même l'urgence de se défendre la révolte a perdu son visage n'oubliez jamais que Rosa Luxemburg écrivait des lettres d'amour n'oubliez pas l'amour la qualité première et non suffisante d'un révolutionnaire sa qualité première la révolte est devenue vaine elle a oublié le chant et la prière elle a négligé le couchant l'aube seulement métaphorique les lendemains qui crissent la révolte est devenue triste la révolte est une grammaire la révolte avance masquée et blême tant de foi révolutionnaire dans la lenteur émue d'un paysan que je connais quand il souffle son labeur tranquille quand il souffle sur sa terre refusant de plier l'échine le couchant est en son cœur non sur l'affiche des tempêtes prédictives rouges sont nos pierres rouges sont nos fleurs rouges nos terres rouges nos couchants rouges nos chants rouges nos prières rouges nos silences rouge le vent rouge la mer et tout s'évanouit lentement sans se presser lentement tout prend la mesure de notre douleur tout parle le langage des morts il fait nuit maintenant.

</div>
<div class="texte" data-name="Phrases G">


### G

Bien sûr il y a le voisinage des champs d'oignons c'est très sérieux les champs d'oignons à l'ombre des platanes bordant l'Hérault sur des sentiers calcaires une horde d'enfants barbouillant dans l'eau plantes invasives habitant la lumière au loin on devine la vallée des jumeaux on entend vrombir les petites locomotives de l'industrie minière vallée fantôme n'est-ce pas plein de rouille et de châtaigniers et de sable rouge ne pas s'y attarder ce qu'on raconte ici m'a fait frémir revenons sur l'allée aux norias assénant jour après jour la nécessité du partage des eaux tout juste un petit saut depuis notre fournaise vers ces terres où pourquoi écrivons-nous je voudrais retrouver notre parcelle retrouver notre vie recommencer la sienne qu'elle vive contenir la mort dans la phrase contenir les champs d'oignons et de betteraves ensoleiller la terre ci-gît le poème d'être partie trop tôt irons-nous vers l'inconnu peut-être consacrer d'autres mystères qu'importe je n'ai jamais tenu bien longtemps l'épée sanguinolente des sciences repues on me parle ici de parchemins de langues mortes d'espaces aléatoires dans la phrase vénérant des rivières d'or et de plomb en finir avec le culte des révélations elle est là gisante notre révélation elle pue le charnier et la braise se consacrer en silence au temps perdu retrouvé perdu perdu perdu vers nous je crois il n'y a que nous rien que nous sur la terre rien que des cartes sans usage rien que ruines et soleil enfoui de la terre continuer à sous un rapport nouveau c'est la promesse d'une terre qui chante dans nos têtes depuis les tout premiers pèlerinages de Paul l'envers et l'endroit de l'universel bon camarade atroce pharisien pêle-mêle il faut creuser creuser pour la rose qui viendra malgré ciel étranglé que la lumière est belle Damas nous marchons vers toi en quête d'une vision nous marchons vers toi Damas donne-nous le courage de troquer notre savoir contre fièvres et marées le courage de refuser la gloriole l'obstination d'aimer enlève-nous l'épine de l'œil affreux qui s'acharne ôte-nous l'obsession des miroirs tout a déjà été écrit ce pourquoi il faut continuer continuer Damas et nous reverrons un jour le soleil se coucher sur notre parcelle mais pour cela il faut continuer Damas continuer non seulement pour le tombeau non seulement par la mémoire c'est notre terre qui gronde il faut continuer pour tes enfants Damas pour les morts Damas pour les vivants Damas continuer.

</div>
<div class="texte" data-name="Phrases H">

### H

Dieu que c'est bête la littérature chacun négociant la reconnaissance de sa petite dette de lumière Dieu que c'est bête tout ça j'ai fait ma valise avec beaucoup de minutie nous avons pris la A6 en direction de Paris tombés en panne à mi-chemin l'alternateur s'est éteint ce qui aurait pu nous coûter la vie sur une aire d'autoroute panne sèche donc et la constellation de Nina libre à chacun d'interpréter des signes moi je m'en remets au chemin il y a des espaces non clos qui je crois échappent à la loi nous n'irons pas à Paris et dire que j'avais rempli ma valise de gros pulls et de livres Proust Beckett Rilke j'ajoute à cela une histoire de Carthage une histoire de la Sicile une histoire des premiers chrétiens et les trois livres de poche rassemblant tous les articles de Walter Benjamin sur le chemin j'ai rencontré Paul Celan de terre d'argile de poussière on dira que j'étale le fond de ma valise c'est plus honnête pourtant de les fondre tous ces noms dans ma phrase je n'écris pas de nulle part mon amour s'enracine dans des traditions toutes ces voix parlent ici elles vivent et j'ai oublié Flaubert dont la pauvreté des métaphores est une richesse j'ai formé ma propre voix avec la matière de leur syntaxe de leur rêve de leurs images afin de trouver la mienne la singularité derrière le symptôme disent les nouveaux prophètes que ferons-nous de tous ces livres des carnets peut-être où inscrire entre les lignes nos contes et nos poèmes nos dessins et de petits mots d'amour pleins de présages de pétales et de pierres un jour j'abandonnerai tout ça et je prendrai les armes un jour je ne crois pas nous avons baptisé notre voiture Ceci ce qui signifie pois chiches en italien hors de question de lui en vouloir nous repartirons demain sur de petites départementales nous repartirons demain soleil d'hiver brûle enfin nous repartirons demain il est trop tôt pour vider la chambre nous repartirons demain je ne veux pas éteindre je ne veux pas la flamme qui danse je ne veux pas il est trop tôt demain pour voir la chambre.

</div>
<div class="texte texte--aleatoire" data-name="Phrases I">


### I

Il y a des fantômes qui me détestent ne cessent de me donner l'ordre de me pendre au ciel moi qui veux seulement dessiner des papillons sur le sol quand ces atroces fantômes m'en empêchent pourquoi toutes ces idées je n'ai que la poésie pour me salir qu'ils aillent se promener comme des touristes loin bien loin de ma tête m'oublier ce sont de pures fictions un peu lourdes comme le sont les romans d'amour à la différence remarquable que mes fantômes rient dans leur cape d'atroce fantaisie fantômes impudiques revenus manger mon foie sales fantômes pourris qu'ils laissent enfin mon cerveau en paix j'ai déposé les armes silence.

</div>
<div class="texte texte--aleatoire" data-name="Phrases J">

### J

Ma petite fleur d'hiver je ne prétends pas que ce papillon saura te guider efficacement parmi les nuages ni qu'il soit en mesure de rendre même imparfaitement hommage à la lumière j'ai perdu ma boussole et me méfie assez des métaphores quant à la lumière on ne peut pas dire aujourd'hui que le climat soit particulièrement propice mais je t'aime oui tant et mal sans doute mais je suis plein d'amour et quand tu es partie j'ai promis de prendre soin de celles et ceux qui s'obstinent à nous tenir compagnie alors pourrais-tu s'il te plaît et quand tu t'en sentiras prête m'aider à veiller sur notre papillon qu'il puise la force de se déployer à travers ciels à travers champs de parcourir sans encombre ni ressentiment notre saison des petites espérances qu'il chérisse ainsi l'éternel recommencement du souffle et de la vision traçant sillon de tes beaux yeux noirs à la faune indomptée du temps.

</div>
<div class="texte texte--aleatoire" data-name="Phrases K">


### K

Aube rond-point périphérie rond-point périphérie rond-point départementale nationale autoroute autoroute autoroute nationale départementale parking sentier marche ascension marche montagne marche contrée marche lisière marche frontière marche oppression marche respiration marche oppression marche respiration marche repos marche village bus village bus parking départementale nationale départementale périphérie parking bus métropole marche irrévérence marche négligence marche outrecuidance marche alcool marche repos marche bus parking départementale nationale autoroute autoroute autoroute nationale départementale parking sentier marche lande marche falaise marche océan marche brume marche persévérance marche éclaircie marche mémoire marche marche marche parking départementale nationale autoroute averses autoroute appréhension autoroute patience autoroute averses autoroute patience autoroute station-service autoroute autoroute autoroute autoroute autoroute autoroute nationale départementale périphérie rond-point périphérie rond-point crépuscule rond-point avenue embouteillage rue stationnement immeuble escalier 1er étage porte appartement entrée salon chambre.

</div>
<div class="texte texte--aleatoire" data-name="Phrases L">


### L

J'ai vu notre papillon se débattre dans une toile d'araignée aucune envie de me ruiner en accusations faciles je laisse Dieu tranquille permettez-moi de m'en prendre aux hommes coupables d'avoir tissé la toile dans laquelle le regard se corrompt se change en calice empoisonné fermente et crache sa petite bravade volubile moisissure laissons Dieu tranquille et le Diable c'est en nous que se joue l'histoire avec nos pieds nos mains nous offrons à aimer et à boire refuser la morale c'est accepter d'agir à la surface des choses c'est-à-dire ici-bas la prière ne suffit plus même inversée même moderniste même franciscaine c'est en invoquant la nuit ses monstres et ses clartés bizarres plongeant la langue dans l'éternelle indécision des possibles mais surtout et conjointement en travaillant honnêtement à une tâche précisément utile que le monde redeviendra monde voilà sages-femmes puéricultrices pédiatres psychologues cliniciennes aides-soignantes infirmières je vous rends justice femmes des nuits où se côtoient le rêve et son envers laissez-moi vous remercier dans cette phrase que personne ne lira vous remercier de l'avoir honorée dans la nuit dans l'amour aucun fantôme atroce ne danse dans les corridors de ma vie déblayons les caves pour nous nourrir du vin de la reconnaissance il y a un monde entier à réparer de haute lutte l'offense est générale et commence dans l'oubli des monstres des saintes s'engager sur les deux fronts la pauvreté de la langue nuit au langage des justes j'ai vu des camarades insulter l'aurore en abolissant le rapport sacré de la pierre et de ses pétales en martelant poncifs clous dans la gorge la certitude surtout quand le poème doute ou se révulse l'envers des yeux crevés c'est la parole muette des chalutiers ports de Bretagne ports de semence que la pierre s'unisse au voyage et casse torpeur de ceux qui se lamentent dans la glace nos rêves ont la douceur des fleurs qui cajolent mains de la somnolence rivées au radeau de l'enfance mort dans la cadence inquiète des relèves ce poème ne pourra pas finir pas finir non qu'il y ait trop à dire mais dans l'écart se loge tu sais de nouveaux rapports non seulement dans le poème dont l'écart est la loi mais dans l'écart c'est-à-dire aussi dans le poème qui maltraite la volupté du sens essayer simplement essayer de retenir le signe dans l'écart il y aura dans l'écart il y a peut-être une fois notre monde.

</div>
<div class="texte texte--aleatoire" data-name="Phrases M">

### M

Un lapin court dans un champ de blé il court et s'égosille personne ne l'entend je ne l'entends pas s'égosiller il court et m'indiffère quelle tristesse ce lapin c'est le lapin de ma sœur cadette il court et court ce lapin enfin il ne court plus puisqu'il est mort depuis 20 ans au moins et dire qu'à l'époque je me foutais des lapins j'ai même été franchement mauvais avec lui indifférent au mieux je m'en veux depuis qu'un matin j'ai vu de petits lapins sauvages courant aux quatre coins de mon jardin je n'ai pas de jardin c'est une formule disons que je les ai vus courir depuis mon lit puisqu'il n'y a pas de lapins qu'il n'y a plus rien je doute même de la réalité d'un lit où suis-je où vais-je le poème n'imagine rien il crève avec tous mes efforts et le bruit de la mer je n'ai pas le souci des insomnies je ne dors pas c'est plus simple pas le souci de la nuit c'est encore plus limpide plus de nuit du tout plus trop de jour non plus j'aimerais trouver la force de faire un bon ménage pour ça je dois me dégoter une chambre plus de chambre plus de travail j'écris des phrases pour tromper l'absente j'écris des phrases et je me lamente quelle tristesse si on pouvait tout recommencer je crois que je me défilerais tout recommencer me paraît fastidieux recommencer les matins recommencer les lapins il ne manquerait plus que de s'imposer la joie de celle d'avant que tout sombre dans l'horreur a-t-on le droit de nos jours d'écrire que nous voulons mourir je ne sais pas je ne suis pas à jour a-t-on le droit de réclamer un ajustement de nos détresses a-t-on seulement le droit de baver comme un nouveau-né ah zut je m'étais promis d'éviter le mot *comme* d'éviter les métaphores rouillées qui n'en sont pas en raison précisément du mot *comme* qui est un outil comparatif qui n'introduit donc pas une métaphore mais une comparaison pourquoi écrivons-nous un beau poème peut-il être utile à celui qui l'écrit j'abolis le point de vue du lecteur car je n'en ai pas ne plus en chercher m'épargne bien des tracas à commencer par la liberté de baver ou celle d'inventer de petits lapins dans un jardin lapins jardin est-ce là dans le fond tout ce que je désire alors je dois arrêter d'écrire et devenir notaire pauvre Flaubert si tu savais je crois qu'il est un peu tard à bientôt trente-quatre ans pour reprendre des études de droit et devenir notaire même si je le souhaitais ce ne serait pas une mince affaire me voici donc avec mes lapins imaginaires auxquels je n'ai rien pas même un coin de terre à offrir rien si ce n'est ce matin-là je crois je me souviens très bien de ce matin-là je les ai vus dès le réveil en poussant la porte de notre chambre c'était juste en face d'un beau volcan c'était l'été car je n'avais pas froid mon abricot dormant paisiblement je me suis aventuré pieds nus dans le jardin entre de petits carrés dédiés à la culture des courgettes ah je me souviens c'était le printemps je crois car il faisait un peu frais en caleçon dans le jardin et j'en déduis donc que ce n'était pas l'été disons même qu'il faisait franchement froid que c'était donc l'hiver en mars précisément que je me suis levé un matin et que j'ai vu plein de petits lapins courir partout ils étaient bruns petits vraiment tout petits et bruns dans des champs qui n'étaient pas de blé mais d'herbe seulement d'herbe un peu fatiguée pas même folle vous savez on use de cette expression d'herbe folle car il faut bien donner une ampleur à la situation c'est un peu malhonnête tout ça et une raison aux hommes de se geler les pieds dans l'herbe à la première heure du jour en ce qui nous concerne c'était un coin de terre recouvert d'herbe inégalement répartie et les petits lapins se faufilant partout aux quatre coins de notre jardin l'herbe semblait remuer de toutes parts pendant que devant moi des rayons précoces jouaient espiègles avec la mer et que le volcan respirait bruyamment vraiment tout pour se faire remarquer ce volcan mais très vite les choses ont évolué le volcan est redevenu pudique un soleil mûr s'est étalé partout sur la mer l'herbe a cessé de remuer et les petits lapins ont disparu voilà je ne sais pas pourquoi cette histoire me bouleverse à ce point.

</div>
<div class="texte" data-name="Phrases N">


### N

En ce premier jour de Carême Jorge Mario Bergoglio appelle les prolétaires du monde entier à s'unir contre la guerre.

</div>
<div class="texte" data-name="Phrases O">


### O

Ficus elastica dans les ruelles dei quartieri spagnoli un baiser tes lèvres sont ce qu'il y a de plus réel 36 variétés d'ail et d'uborka hongrois moi qui ne sais pas même le nom des rues t'en souviens-tu j'ai comme l'intuition que tout a déjà été écrit ô maladroitement sans doute il faut simplement recommencer l'image la préciser maintenant que mes fantômes sont partis je peux je dois assumer un nom au cœur de l'épine dorsale de l'univers il y a des trembles et des acacias rouges recommencer à dire toujours plus besoin de m'inventer des noms pour contourner le centre de nos douleurs c'est un boulet fleuri il faut oser détourner la langue la laisser dire ce qu'on ne voit pas tout commence avec ce qui ne se voit pas les ruelles de notre amour réenchantant les nuits de notre enfance pleines de fougères en fleur sur la petite commode de notre chambre et cette misère t'en souviens-tu rampante dans l'ombre de l'enfant mort et tout le reste et tout le reste c'est-à-dire l'essentiel tout ce qui ne se dit ne peut se dire il faut le dire pour se donner le courage de gravir notre montagne l'aube écarlate l'aube criarde rien n'est jamais certain il faut gravir notre sentier d'amour se pardonner nos blessures et le soufre indigeste et le soufre merveilleux il faut se pardonner dans le jour affamé cruel il faut se pardonner notre amour et ses colliers d'aulx frais dans une rue d'Albaicín te souviens-tu Albaicín et les pluies d'orage à Madrid et cette crêpe au chèvre-miel qu'un routier nous avait offerte et ces ronds-points et ces grimaces de quelques jeunes bourgeois d'Aix-en-Provence t'en souviens-tu et Marseille enfin notre maison notre tombeau notre port il faut tout se pardonner il faut aimer toujours.


</div>
<div class="texte" data-name="Phrases P">

### P

Nous nous sommes aventurés dans la chapelle jésuite au centre de Nîmes chapelle vidée de toute fioriture déserte pas même un banc blanche pétrie de lumière et d'assemblées jacobines en leur écho proche lointain je me suis toujours demandé comment les prêtres-ouvriers d'Argentine pouvaient être les cousins des aristotéliciens aigres de Babylonie c'est peut-être une histoire d'ancrage et les familles heureuses ne sont pas légion dit-on que nous lisions Marx ou ne le lisions pas sans doute que ce n'est plus la même histoire mais la pratique devance le texte sans le contourner elle plonge dans les grimoires des mains de douce compassion consciente prenez les sœurs de Sion qui ont pendant des lustres bâti des écoles sans mesurer l'ampleur des pavés qu'elles jetaient dans la mare je ne sais pas ayant entendu l'homélie de Jorge prononcée à Noël j'ai pleuré à deux mains de lui écrire me ravisant par pudeur quand le poème cloche brisée s'aventure au-delà du soufre et de la plainte nul doute qu'il éclaire la pratique ce pourquoi il n'y a pas de contradiction réelle entre l'implosion du sens et l'œuvre de courage les deux appellent ici demain l'étoile du matin.


</div>
<div class="texte" data-name="Phrases Q">

### Q

C'est un monstre qui marche dans la nuit que personne ne devine allongé sur la grève butiné par la bruine un monstre les pieds gelés l'œil honteux se cache dedans la terre dedans fouille d'entre les morts en cet espace que l'on dit grouillant de petits insectes mauves gonflés du sang de la Castille marche sans le souci des destinations sans jamais se reposer ni s'attendrir trop longtemps poursuivant son œuvre de rédemption ange fatigué et coupable dans la quête toujours n'a jamais vu Paris rôde le long des périphériques arme sa nuit de souffles miroitants jamais trop longtemps dans les cafés de l'alcool facile marche le long des autoroutes s'arrête et crache sur le genêt qu'il aime sur la pierre qui le fascine car il faut à tout prix percer le mystère des corps et de leur inertie marche et tombe dans le fossé qui lorgne la rocade mangeant herbe humide pour mieux la recracher herbe triste dans la nuit des partisans marche sur des terres incendiées et froides entre plaines et monts dans le laboratoire des iris mécanique avalanche beaucoup rogner la tranche crucifier son érable et sa mise décolorer le temps recommencer à sourdre faussaire marche mais ne s'exhibe pas quand bien même il le voudrait personne ne le voit personne pas d'écho invisible n'existe pas marche et ne marche pas c'est un homme et ce n'est pas un homme c'est une femme et ce n'est pas une femme c'est un monstre sans dents sans visage sans jambes sans calendriers sans préavis qui marche dans la nuit.

</div>
<div class="texte texte--aleatoire" data-name="Phrases R">


### R

Ceci est une phrase blanche.

</div>
<div class="texte texte--aleatoire" data-name="Phrases S">


### S

AIDE-MOI À ME COUCHER NU DANS LE LIT DE L'AIMÉE AIDE-MOI À TROUVER LE REPOS AIDE-MOI À CONTEMPLER LES PREMIERS RAYONS DE L'AURORE AIDE-MOI À LES TROUVER BEAUX AIDE-MOI DANS L'ÉPREUVE AIDE-MOI DANS LA JOIE AIDE-MOI À MORDRE DANS LE PAIN DES DÉSHÉRITÉS À GOÛTER ENCORE LE VIN DE L'ESPÉRANCE AIDE-MOI À ME VIDER DE MA RANCŒUR AIDE-MOI À BRISER LE MOT À DEVANCER LE SENS AIDE-MOI À OUVRIR LA BRÈCHE AIDE-MOI À TROUVER LE RAPPORT NOUVEAU AIDE-MOI À DÉSIRER LA PAIX AIDE-MOI DANS L'EFFROI AIDE-MOI DANS LE DOUTE AIDE-MOI DANS L'ATTENTE AIDE-MOI À ME PASSER DE TOI AIDE-MOI À CUEILLIR L'OLIVE MÛRE DE BEAUTÉ AIDE-MOI À DEVENIR UNE ROSE AIDE-MOI À MOURIR AIDE-MOI À LA RETROUVER AIDE-MOI À ME TENIR DEBOUT AIDE-MOI À CONTINUER.

</div>
<div class="texte" data-name="Phrases T">

### T

<span class="alphabet">ለሊት</span> <span class="alphabet">ليل</span> <span class="alphabet">գիշեր</span> <span class="alphabet">gecə</span> gaua <span class="alphabet">রাত</span> оч noć нощ nit gabii usiku notte lannwit noć nat noche öö yö nuit nuit nuit noite neith nuit nuit νιτ nufi nuit nuit <span class="alphabet">निट</span> nuj nuit nuit tidak nuit nuit notte <span class="alphabet">夜</span> wengi <span class="alphabet">ರಾತ್</span> <span class="alphabet">түн</span> ijoro night <span class="alphabet">түн</span> şev <span class="alphabet">ກາງຄືນ</span> nox nakts naktis Nuecht ноќе malam <span class="alphabet">രാത്രി</span> alina natën nag lejl po <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> Nacht <span class="alphabet">रात</span> natt <span class="alphabet">ରାତି</span> <span class="alphabet">رات كېچە</span> tun <span class="alphabet">شپه</span> <span class="alphabet">ਰਾਤ</span> <span class="alphabet">شب</span> noc noite Nacht noapte ночь po ноћ husiku <span class="alphabet">رات</span> noc noč habeen bosiu peuting natt usiku шаб gabi <span class="alphabet">இரவு</span> <span class="alphabet">төн</span> <span class="alphabet">យប់</span> noc <span class="alphabet">రాత్రి</span> <span class="alphabet">กลางคืน</span> gece <span class="alphabet">රෑ</span> 밤 gije ніч đêm busuku <span class="alphabet">נאַכט</span> ale ebusuku nuèit <span class="alphabet">夜晚</span> noite neith nuit nuit νιτ nufi ночь po ноћ husiku <span class="alphabet">رات</span> noc noč habeen bosiu usiku notte lannwit noć nat noche öö tun <span class="alphabet">شپه</span> <span class="alphabet">ਰਾਤ</span> <span class="alphabet">شب</span> lejl po <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> Nacht <span class="alphabet">रात</span> natt <span class="alphabet">ରାତି</span> <span class="alphabet">رات كېچە</span> <span class="alphabet">夜</span> wengi <span class="alphabet">ರಾತ್ರಿ</span> <span class="alphabet">түн</span> ijoro <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> Nacht <span class="alphabet">रात</span> ebusuku nuèit <span class="alphabet">夜晚</span> natën nag lejl po <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> <span class="alphabet">രാത്രി</span> alina natën nag lejl nuit tidak nuit <span class="alphabet">ರಾತ್ರಿ</span> <span class="alphabet">түн</span> ijoro <span class="alphabet">রাত</span> ноч noć нощ nit natën nag lejl nakts naktis <span class="alphabet">夜</span> wengi <span class="alphabet">ರಾತ್ರಿ</span> <span class="alphabet">түн</span> natt <span class="alphabet">ରାତି</span> <span class="alphabet">رات كېچە</span> tun <span class="alphabet">شپه</span> <span class="alphabet">ਰਾਤ</span> natën nag lejl po <span class="alphabet">रात्री</span> <span class="alphabet">шөнө</span> Nacht <span class="alphabet">रात</span> natt <span class="alphabet">ରାତି</span> <span class="alphabet">رات كېچە</span> tun <span class="alphabet">شپه</span> <span class="alphabet">ਰਾਤ</span> <span class="alphabet">شب</span> noc noite Nacht noapte ночь po ноћ husiku.

</div>
<div class="texte" data-name="Phrases U">

### U

J'aimerais revoir Assise afin de vérifier quelque chose les lieux dit-on nous habitent je ne sais pas j'aimerais revoir Assise pour ne plus rien imaginer me confronter à l'appel m'y résoudre ou lutter revoir Assise pour me trouver un paysage revoir Assise au printemps pour constater que la vie recommence égoïstement revoir Assise pour ne plus me débattre quand sonne l'heure du poème acquiescer revoir Assise pour connaître les voies qui mènent à toi pour me trouver le courage de les arpenter revoir Assise pour naître qui sait pour naître une troisième fois revoir Assise pour manger dans tes mains revoir Assise pour demander des comptes et puis pour pardonner au soleil de se lever chaque matin sur nos nuits perpétuelles revoir Assise pour ses oliviers pour sa terre pour ses ruelles et ses marchands de mort pour ses martyrs et nos silences revoir Assise une fois pour étrangler le temps revoir Assise pour pleurer.

</div>
<div class="texte texte--aleatoire" data-name="Phrases V">

### V

J'ai toujours été vache avec les étoiles pardonne-moi un vieux contentieux fallait-il que je te rencontre pour adoucir ma voix et cheminer dans la nuit neuve dans la nuit réelle dans la nuit impartiale fallait-il que tu naisses pour que je naisse aussi consentir à l'appel du minéral qui bat poumon d'un pays assiégé flore d'apôtres j'aime passionnément la nuit depuis qu'elle devance l'ordre des significations être moderne ou ne pas l'être c'est bien peu de chose si le vieux loup chante quelquefois en négligeant la pierre et le marteau le renard tourne vil en couvrant de brisures l'apparat des discours seule importe la voix qui creuse le silence avec son chant sa pierre et son marteau nous nous sentons désormais la force de bâtir des châteaux de signes inviolables sur les contreforts de vieux volcans oubliés une étoile existe dans la nuit qui luit sans sous-entendu une étoile qui ne dit rien passe et file une étoile au cœur de la constellation d'Orion.

</div>
<div class="texte" data-name="Phrases W">

### W

Hier nous sommes descendus sur le chemin de Baume menant à une gorge karstique clarté pauvre et vigoureuse nous nous sommes assis près d'une rivière à laquelle je n'avais jamais parlé Baume et douleur du temps dans l'instant la mémoire sans aucun étalage le vent quadrillant ton sourire qui n'était peut-être pas tout à fait ce qu'on appelle un sourire je t'ai vue dans l'épreuve tellement loin des grimaces de l'événement tellement proche du cœur infaillible de notre garrigue je t'ai vue d'un œil qui n'est peut-être pas tout à fait l'œil qui regarde j'ai vu l'instant qui condense tous les instants sans aucune trame sans formalisme sans rien qui ait à voir avec un récit ou un exercice de la mémoire sans rien que l'on puisse nommer littérature on dit que Giambattista Marino à la veille de sa mort découvrit enfin la rose qu'il avait passé sa vie à tenter de contenir dans le poème qu'il la vit réellement pour la première fois une rose jaune échappant à toute littérature alors me vint une idée qu'il faudrait simplement raconter l'échec de la phrase et puis je me suis souvenu que ce n'était pas une idée neuve que Proust et Beckett n'avaient fait que ça chercher retenir constater l'échec merveilleux du dire et de l'écrire à l'aune de l'œil qui perçoit ce que la langue ne pourra jamais restituer laisse-moi une fois tourner autour de ton immense douleur tourner avec le vent autour du gisement de ta vie ce dévouement à la nécessité cette audace et ce courage du corps cette vérité de la pensée pudique ta force évoquer le lien d'amour infini qui vous lie je n'en suis pas capable un jour peut-être en attendant laisse-moi tourner autour de ton silence le jour s'efface lentement sur la roche les quelques jeunes environnants ont fini de parler fort la rivière livre ses derniers éclats de feu discret nous sommes seuls.

</div>
<div class="texte" data-name="Phrases X">

### X

Les enfants d'Auguste ne sont pas nos enfants faut-il partir ou rester le temple de Diane n'est pas un temple et ne fut jamais consacré à la mémoire de Diane les archéologues nous mentent je lis Baudelaire et me rends bien compte que le voyage est affaire de répétition et de privilège que l'on parte que l'on reste c'est la même image et la même sentence qui nous cisaillent avec nos gueules d'Albatros n'ayant jamais appris à chasser crois-tu que nous saurons nous débrouiller puisqu'à la fin ne reste que cendre et mémoire partons il existe des chapelles assez grandes pour contenir notre douleur des routes assez larges pour casser de la brique sur le front des collines des colères de lunes ensanglantées des épitaphes à inscrire partout sur chaque pot de fleurs chaque pavé je me fous de revoir Naples je veux seulement étourdir ma peine dans le trafic des pierres et des violettes puisque nous savons désormais l'inébranlable destin de la rose contentons-nous de danser c'était un soir de Noël devant l'église de Saint-Ferréol à Marseille nous avons dansé sur le port dansé sous la pluie devant des passants ébahis qui ont dû croire que nous étions ivres avant de plonger de nouveau dans le secret de ces nuits d'hiver dans un quartier plein de boutiques hideuses il est probable cette fois que nous échappions aux résolutions aux vœux c'est une aubaine dans le décor des impatients Castellane brûle je crois même que le ciel verdit des poèmes saltimbanques s'affichent à l'entrée de l'hôpital de la Conception faut-il préciser que Rimbaud y est mort avec sa seule jambe et le souffle court il y avait également des rats de la taille d'une voiture et de tout petits monstres sous nos semelles et des cris échappés du bâtiment dédié aux patients atteints de troubles neurologiques nous nous sommes agenouillés dans l'herbe sur le boulevard Baille et l'herbe était très douce puis nous avons osé un rêve de silence et de paix avec un cuirassé d'Odessa tatoué sur la poitrine la cave étant vide nous sommes rentrés dans notre grotte au 8e étage d'un immeuble un peu gris et nous avons ouvert des conserves d'Arménie et j'ai bu dans ta paume de ce vin de Grenade tout est vrai ici tout est vrai je n'invente rien seulement je ne me souviens plus du moment où nous avons fermé l'œil.

</div>
<div class="texte" data-name="Phrases Y">

### Y

Ce que la phrase dit ou ne dit pas ce qu'elle arrive à dire ce qu'elle n'arrive pas à dire ce qu'elle dit sans le dire ce qu'elle ne dit pas en croyant le dire ne nous appartient pas.

</div>
<div class="texte" data-name="Phrases Z">

### Z

Sur une table de bistrot à Nîmes il y a mon tabac mon café allongé plus économique et plus long à savourer et il y a moi qui n'ai jamais été aussi loin de moi jamais aussi proche cela fait trois mois hier l'image qui habite toutes mes nuits est celle de la première fois quand je l'ai portée dans mes bras quand elle a posé sa petite main sur mon torse ma compagne émue et moi faisant le fier comme si je n'avais pas peur et en vérité je n'avais pas peur cela a duré une heure ou une heure et demie ou deux heures moins peut-être ou plus longtemps je ne sais pas tout mon désir étant ici contenu dans la présence juste et infalsifiable il y a longtemps j'ai peut-être ressenti quelque chose d'analogue en regardant la mer c'était à Syracuse mais j'étais un peu ivre et seul tandis que ce jour-là tenant mon enfant dans les bras auprès de ma compagne je n'étais pas seul j'étais avec mes deux amours et croyais alors que cet instant prenant une infinité de formes contingentes se prolongerait bien des années bien des années encore je ne veux pas imaginer ces formes et ces années aujourd'hui à ma table de bistrot à Nîmes je pleure et si je sais bien que ma douleur a un avenir je ne veux surtout plus rien imaginer pour en fixer le seuil pour la figer dans la perspective de son abolition cette douleur qui est notre feuillage d'hiver comme le murmure Rilke car c'est en elle que se loge l'âme des êtres les plus précieux et les plus éphémères et tout notre amour et la joie un jour ici toute l'œuvre de Rilke gravite autour de cet espace invisible qui lie sans les confondre les vivants et les morts Rilke dont le prénom porte la marque d'un enfant mort de sa grande sœur Maria née seulement une année avant lui et dont les *Élégies* sont le tombeau et le jardin et la baie et le château autant de signes autant de noms qui tiennent fragiles dans le poème dans ce qui n'est pas je crois une œuvre d'hommage mais une prière païenne pour la seule raison que ce qui la soutient et la justifie n'est pas une Église ou l'ordre d'un discours mais une âme libre assumant le manque au cœur de ses entrailles qui ose appeler sans savoir si on lui répondra qui ose appeler car l'appel est tout ce que nous avons et qu'il est tout ce dont nous avons besoin que l'appel ne s'effraie de rien pas même du désert qui s'en fait l'écho pas même des railleries pas même des charlatans qui prétendent apporter une réponse à notre déshérence à notre agonie à Nîmes aujourd'hui ni l'azur ni la pluie ne pourront rafistoler notre décor c'est un matin irrémédiablement triste les quelques passants affairés je ne les connais pas pourtant j'aimerais les considérer comme des amis ainsi que ce vieil arbre au loin à l'embouchure du boulevard Victor Hugo entre la Maison carrée et la bibliothèque municipale ce vieil arbre nu qui résiste au temps sans nous juger derrière nous il y a un canal menant au Jardin de la fontaine il y a un temple défraîchi dont on dit qu'il fut une bibliothèque au sein du sanctuaire dédié à Auguste et il existe une source antique également au cœur de ce jardin mais on ne peut la voir les hommes l'ont tarie puisqu'elle était devenue inutile jadis elle était pourtant le centre de la vieille cité antique de Nîmes et je ne sais pas pourquoi je raconte tout ça sans doute parce que je voudrais m'accrocher aux ruines pour en révéler la mémoire désormais parce que je voudrais me rassurer en nommant les éléments d'un décor qui n'est plus celui des vivants qui est celui des vivants et des morts parce que je veux m'assurer que les lieux même défraîchis même oubliés continuent à servir de monde à celles et ceux qui ne sont plus que ces lieux nous engagent ainsi à nous souvenir des vivants.

</div>
<div class="texte" data-name="Domus de Janas">

## Domus de Janas

<div class="centre">
toutes les nuits

sur le sable rouge

d'une île

à la bouche fumante

<div>&nbsp;</div>

nous écrivons

<div>&nbsp;</div>

lumière

en

arabe

<div>&nbsp;</div>

mandarinier

en

araméen

<div>&nbsp;</div>

amour

en

provençal
</div>

</div>

