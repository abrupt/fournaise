// Lazy

// const optionsLazy = {
//   // Your custom settings go here
//   elements_selector: '.lazyload',
//   // Native lazy loading
//   // use_native: true
//
// }
// const lazyLoadInstance = new LazyLoad(optionsLazy);

//// Menu
const menuBtn = document.querySelector(".button--menu");
const menu = document.querySelector(".menu");
let menuOpen = false;

const container = document.querySelector('.container');
const textes = [...document.querySelectorAll(".texte")];
const texteContainer = document.querySelector(".textes");
let textesOpen = false;

const loadBtn = document.querySelector(".btn--load");

loadBtn.addEventListener("click", (e) => {
  e.preventDefault();
  loadBtn.blur();
  document.querySelector('.loading').classList.add("loading--done");
});

menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuBtn.blur();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.enabled = false;
  } else {
    // if (!isTouchDevice()) controls.enabled = true;
  }
  menu.classList.toggle("show--menu");
});


//
// Scripts hack
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var content = document.querySelector('.container');
// content.addEventListener('touchstart', function (event) {
//     this.allowUp = (this.scrollTop > 0);
//     this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
//     this.slideBeginY = event.pageY;
// });
//
// content.addEventListener('touchmove', function (event) {
//     var up = (event.pageY > this.slideBeginY);
//     var down = (event.pageY < this.slideBeginY);
//     this.slideBeginY = event.pageY;
//     if ((up && this.allowUp) || (down && this.allowDown)) {
//         event.stopPropagation();
//     }
//     else {
//         event.preventDefault();
//     }
// });

// Shuffle array
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

// Random number
function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}
function randomNbFrac(min, max) {
  // min and max included
  return Math.random() * (max - min) + min;
}

// Map
const map = L.map('map').setView([44.214, 16.501], 5);

let carte;

// carte = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}{r}.png', {
carte = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
	subdomains: 'abcd',
});

// carte = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}{r}.{ext}', {
// 	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
// 	subdomains: 'abcd',
// 	minZoom: 0,
// 	maxZoom: 20,
// 	ext: 'png'
// });

// carte = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
// 	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
// 	subdomains: 'abcd',
// 	minZoom: 1,
// 	maxZoom: 16,
// 	ext: 'jpg'
// });

carte.addTo(map);

// const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
//   // maxZoom: 19,
// 	}).addTo(map);

document.querySelector('.leaflet-control-attribution').style.display = "none";

var locationIcon = L.icon({
    iconUrl: 'etc/zone.svg',
    // shadowUrl: 'etc/marker-shadow.png',

    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    // iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const locations = [
  {
    name: 'Sans emploi',
    place: 'Les arènes de Nîmes',
    coord: [43.83484, 4.35962]
  },
  {
    name: 'Vallée fantôme',
    place: 'Les halles de Ganges',
    coord: [43.93453, 3.7091]
  },
  {
    name: 'Os',
    place: 'Place de l’horloge à Nîmes',
    coord: [43.83812, 4.35843]
  },
  {
    name: 'Cendre',
    place: 'Cimetière de Portbou',
    coord: [42.42686, 3.16336]
  },
  {
    name: 'Notes sur l’amour',
    place: 'Marché de Naples',
    coord: [40.84437, 14.24942]
  },
  {
    name: 'Le livre de Nina',
    place: 'Calanque de Callelongue à Marseille',
    coord: [43.21208, 5.35291]
  },
  {
    name: 'Sentier',
    place: 'Cirque d’Auch',
    coord: [43.65224, 0.59289]
  },
  {
    name: 'Phrases C',
    place: 'Port de Dublin',
    coord: [53.3459, -6.1899]
  },
  {
    name: 'Phrases H',
    place: 'Départementale 179',
    coord: [43.4508, 2.9076]
  },
  {
    name: 'Phrases T',
    place: 'Cratère du Teide',
    coord: [28.2721, -16.6423]
  },
  {
    name: 'Phrases F',
    place: 'Port de Collioure',
    coord: [42.5283, 3.0875]
  },
  {
    name: 'Phrases E',
    place: 'Côte sauvage dans le Morbihan',
    coord: [47.5236, -3.1589]
  },
  {
    name: 'Phrases O',
    place: 'Quartier Albaicín à Grenade',
    coord: [37.18275, -3.59135]
  },
  {
    name: 'Phrases D',
    place: 'Carthage à Tunis',
    coord: [36.8656, 10.3299]
  },
  {
    name: 'Phrases Q',
    place: 'Pont Mirabeau à Paris',
    coord: [48.84665, 2.27559]
  },
  {
    name: 'Phrases G',
    place: 'Centre de Damas',
    coord: [33.5128, 36.3096]
  },
  {
    name: 'Phrases V',
    place: 'Cratère du Vulcano',
    coord: [38.40326, 14.96543]
  },
  {
    name: 'Phrases N',
    place: 'Saint-Siège à Rome',
    coord: [41.89483, 12.5842]
  },
  {
    name: 'Phrases U',
    place: 'San Damiano à Assise',
    coord: [43.06467, 12.6189]
  },
  {
    name: 'Phrases P',
    place: 'Chapelle jésuite à Nîmes',
    coord: [43.83724, 4.36247]
  },
  {
    name: 'Phrases W',
    place: 'La baume dans le Gard',
    coord: [43.93874, 4.43122]
  },
  {
    name: 'Phrases X',
    place: 'Hôpital de la Conception à Marseille',
    coord: [43.29028, 5.39562]
  },
  {
    name: 'Phrases Z',
    place: 'Temple de Diane à Nîmes',
    coord: [43.83987, 4.34852]
  },
  {
    name: 'Domus de Janas',
    place: 'Punta La Marmora',
    coord: [39.98786,9.32554]
  }
]

let passagesAleatoires = document.querySelectorAll('.texte--aleatoire');

let locationsOnMap = [];
let locationsOnMapRandom = [];
locations.forEach((el) => {
  const place = L.marker([el.coord[0],el.coord[1]], {icon: locationIcon}).addTo(map);
  locationsOnMap.push(place);
});

passagesAleatoires.forEach((el) => {
  const place = L.marker([randomNbFrac(43.614,49.268),randomNbFrac(-0.5,5.9)], {icon: locationIcon}).addTo(map);
  locationsOnMapRandom.push(place);
});

locationsOnMap.forEach((el) => {
  el.addEventListener('click', (ev) => {
    if (textesOpen) {
      document.querySelector(".texte.show").scrollIntoView();
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      const placeInArray = locationsOnMap.indexOf(el);
      const name = locations[placeInArray].name;
      const texte = document.querySelector('[data-name="' + name + '"]')
      texte.classList.add("show");
    } else {
      const placeInArray = locationsOnMap.indexOf(el);
      const name = locations[placeInArray].name;
      const texte = document.querySelector('[data-name="' + name + '"]')
      texte.classList.add("show");
      texteContainer.classList.add("show");
      textesOpen = !textesOpen;
    }
  });
});

locationsOnMapRandom.forEach((el) => {
  el.addEventListener('click', () => {
    if (textesOpen) {
      document.querySelector(".texte.show").scrollIntoView();
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      const placeInArray = locationsOnMapRandom.indexOf(el);
      const passage = passagesAleatoires[placeInArray];
      passage.classList.add("show");
    } else {
      const placeInArray = locationsOnMapRandom.indexOf(el);
      const passage = passagesAleatoires[placeInArray];
      passage.classList.add("show");
      texteContainer.classList.add("show");
      textesOpen = !textesOpen;
    }
  });
});

const closeBtn = document.querySelector(".textes__close");
closeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".texte.show").scrollIntoView();
  texteContainer.classList.remove("show");
  textes.forEach((el) => {
    el.classList.remove("show");
  });
  textesOpen = !textesOpen;
  closeBtn.blur();
});

var group = new L.featureGroup(locationsOnMap);
map.fitBounds(group.getBounds());
let southWest = L.latLng(-90, -180);
let northEast = L.latLng(90, 180);
let bounds = L.latLngBounds(southWest, northEast);
map.setMaxBounds(bounds);
map.setMinZoom(3);

//
// Custom cursor
//
const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('a, input, button, .button, .leaflet-marker-icon');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();

// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
     cursorBg.classList.add('cursor--interact');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
     cursorBg.classList.remove('cursor--interact');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');
   cursorBg.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
     cursorBg.classList.remove('cursor--click');
   }, 250);
 });


//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector(".menu"));
let simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
// Disable scroll simplebar for print
window.addEventListener("beforeprint", (event) => {
  simpleBarContent.unMount();
  simpleBarContentTextes.unMount();
});
window.addEventListener("afterprint", (event) => {
  simpleBarContent = new SimpleBar(document.querySelector(".menu"));
  simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
});





