# ~/ABRÜPT/PIERRE-AURÉLIEN DELABRE/FOURNAISE/*

La [page de ce livre](https://abrupt.cc/pierre-aurelien-delabre/fournaise/) sur le réseau.

## Sur l'ouvrage

FOURNAISE prend acte de l’épuisement du sens tel que l’Histoire des vainqueurs nous l’inflige et quête une revitalisation de l’épopée minuscule.

Cette quête ne vise pas tant ici la difficile dialectique de la pesanteur et de la grâce (le concept et sa brèche poétique) que l’hypothèse d’une véritable "indistinction du récit et de la théorie" (Adorno, Notes sur Beckett) — et cela via l’exploration des formes les plus novatrices du genre romanesque, dans leur façon de fabriquer du roman tout en en explicitant les limites.

Ainsi, en adeptes du braconnage, nous n’hésiterons guère à puiser dans certaines techniques narratives — émeute de détails, flux de conscience, style bureaucratico-documentaire — en vue de traiter sur le terrain de la recherche poétique des problématiques aussi universelles et prosaïques que la répartition des tâches ménagères, la grossesse, la paternité, la mort.

FOURNAISE est à peu près tout sauf un roman — c’est le déploiement polymorphe et balbutiant d’un presque rien auquel nous tenons — notre raison de continuer.

## Sur l'auteur

Né en 1988 en région parisienne.<br>
Fabrique des textes et des images.<br>
Habite aujourd’hui à Nîmes.

Son site : [Esprit de l'utopie](https://espritdelutopie.com/).

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International ([CC-BY-NC-SA 4.0](LICENSE-TXT)).

La version HTML de cet antilivre est placée sous [licence MIT](LICENSE-MIT). Elle fonctionne grâce aux outils libres <a href="https://leafletjs.com/">Leaflet</a> et <a href="https://www.openstreetmap.org">OpenStreetMap</a>.

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
